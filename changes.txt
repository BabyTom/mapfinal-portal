
-----------------------------
mapfinal-0.1:

新增：新增Portal门户
新增：新增LocalServer地图服务器，采用GeoTools开发
新增：新增GeoServer-Manager代码
新增：新增GeoServer、百度BMap、高德AMap、天地图的地图服务器管理
新增：新增shape文件管理支持
新增：新增postgis空间数据管理支持
新增：新增GeoApiService实现
优化：升级lambkit到1.0.2版本
优化：优化门户系统为lambkit-zheng代码
优化：优化CPATH为TPATH并关联TemplateManager
优化：优化模板文件配置和更新
优化：优化MapApi实现方式
优化：优化PostgisStore的name设置
修复：修复ShiroSsoInterceptor跳转问题