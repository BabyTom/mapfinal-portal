# GeoApi功能

### 介绍

mapfinal提供了api服务功能

### 使用方法

1、通过http-json方式获取api服务

2、通过RPC方式获取api服务

#### http-json方式

以dwithin接口服务为例，调用如下：

http://url:port/api/dwithin/main:mapTownPoint-0101000000410702092F745D406A14D4E8BCFA3F40-2000

http://url:port/api/dwithin?layer=main:mapTownPoint&geometry=POINT(117.8153708%2031.97944503)&radius=2000

#### RPC方式

待添加

### Api说明

	/**
	 * 等同于PostGIS函数ST_Distance(geometry, geometry)
	   返回两个几何对象的距离（笛卡儿距离），不使用索引。
	 * layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "distance")
	public List<Record> distance(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_DWithid(geometry, geometry, float)
	如果一个几何对象(geometry)在另一个几何对象描述的距离(float)内，返回TRUE。如果有索引，会用到索引。
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 * dist 距离值，单位米
	 */
	@ApiMapping(value = "dwithin")
	public List<Record> dwithin(String layerName, String geometry, float dist);
	
	/**
	 * 等同于PostGIS函数ST_Equals(geometry, geometry)
	如果两个空间对象相等，则返回TRUE。用这个函数比用“=”更好，例如：
	equals(‘LINESTRING(0 0, 10 10)’,‘LINESTRING(0 0, 5 5, 10 10)’) 返回 TRUE。
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "equals")
	public List<Record> equals(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Disjoint(geometry, geometry)
	如果两个对象不相连，则返回TRUE。不要使用GeometryCollection作为参数。
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "disjoin")
	public List<Record> disjoin(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Intersects(geometry, geometry)
	判断两个几何空间数据是否相交,如果相交返回true,不要使用GeometryCollection作为参数。
	Intersects(g1, g2 ) --> Not (Disjoint(g1, g2 ))
	不使用索引可以用_ST_Intersects.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "intersects")
	public List<Record> intersects(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Touches(geometry, geometry)
	如果两个几何空间对象存在接触，则返回TRUE。不要使用GeometryCollection作为参数。
	a.Touches(b) -> (I(a) intersection I(b) = {empty set} ) and (a intersectionb) not empty
	不使用索引可以用_ST_Touches.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "touches")
	public List<Record> touches(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Crosses(geometry, geometry)
	如果两个几何空间对象存在交叉，则返回TRUE。不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Crosses.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "crosses")
	public List<Record> crosses(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Within(geometry A, geometry B)
	如果几何空间对象A存在空间对象B中,则返回TRUE,不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Within
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "within")
	public List<Record> within(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Overlaps(geometry, geometry)
	如果两个几何空间数据存在交迭,则返回 TRUE,不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Overlaps.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "overlaps")
	public List<Record> overlaps(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Contains(geometry A, geometry B)
	如果几何空间对象A包含空间对象B,则返回 TRUE,不要使用GeometryCollection作为参数。
	这个函数类似于ST_Within(geometry B, geometryA)
	不使用索引可以用_ST_Contains.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "contains")
	public List<Record> contains(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_Covers(geometry A, geometry B)
	如果几何空间对象B中的所有点都在空间对象A中,则返回 TRUE。
	不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Covers.
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "covers")
	public List<Record> covers(String layerName, String geometry);
	
	/**
	 * 等同于PostGIS函数ST_CoveredBy(geometry A, geometry B)
	如果几何空间对象A中的所有点都在空间对象B中,则返回 TRUE。
	* layerName 图层坐标，main:mapTownPoint形式为（store名称）:（图层名称）
	 * geometry 图形坐标，采用wkt或wkb参数
	 */
	@ApiMapping(value = "coveredby")
	public List<Record> coveredby(String layerName, String geometry);
	

