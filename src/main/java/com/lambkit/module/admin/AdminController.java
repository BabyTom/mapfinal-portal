package com.lambkit.module.admin;

import com.lambkit.web.controller.LambkitController;

public class AdminController extends LambkitController {

	public void index() {
		setAttr("adminUrl", AdminManager.me().getWebConfig().getUrl());
		renderTemplate(AdminManager.me().getTemplatePath() + "/index.html");
	}
	
	
}
