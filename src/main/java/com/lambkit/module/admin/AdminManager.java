package com.lambkit.module.admin;

import com.lambkit.web.WebConfig;
import com.lambkit.web.WebConfigManager;

public class AdminManager {

	private static AdminManager manager = null;
	
	public static AdminManager me() {
		if(manager==null) {
			manager = new AdminManager();
		}
		return manager;
	}

	private WebConfig webConfig;
	
	public AdminManager() {
		webConfig = WebConfigManager.me().getWebConfig("admin");
		if(webConfig==null) {
			webConfig = new WebConfig();
			webConfig.setPath("/WEB-INF/admin");
			webConfig.setTemplate("zheng");
			WebConfigManager.me().putWebConfig("admin", webConfig);
		}
	}
	
	public WebConfig getWebConfig() {
		return webConfig;
	}
	
	public void setWebConfig(WebConfig webConfig) {
		this.webConfig = webConfig;
		WebConfigManager.me().putWebConfig("admin", webConfig);
	}
	
	public String getTemplatePath() {
		return AdminManager.me().getWebConfig().getPath()  + "/" + AdminManager.me().getWebConfig().getTemplate();
	}
}
