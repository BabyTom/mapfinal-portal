package com.lambkit.module.admin;

import com.jfinal.config.Routes;
import com.lambkit.module.LambkitModule;
import com.lambkit.web.WebConfig;

public class AdminModule extends LambkitModule {

	@Override
	public void configRoute(Routes me) {
		WebConfig config = AdminManager.me().getWebConfig();
		String ckey = config.getUrl();
		me.add(ckey, AdminController.class);
	}
	
}
