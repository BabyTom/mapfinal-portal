package com.lambkit.module.cms;

import com.jfinal.config.Routes;
import com.lambkit.common.util.StringUtils;
import com.lambkit.module.cms.common.CmsConfig;
import com.lambkit.module.cms.web.controller.BlogController;
import com.lambkit.module.cms.web.controller.IndexController;
import com.lambkit.module.cms.web.controller.NewsController;
import com.lambkit.module.cms.web.controller.PageController;
import com.lambkit.module.cms.web.controller.QaController;
import com.lambkit.module.cms.web.controller.SearchController;
import com.lambkit.module.cms.web.controller.TopicController;
import com.lambkit.module.cms.web.interceptor.CmsWebInterceptor;

public class CmsWebRoutes extends Routes {

	@Override
	public void config() {
		addInterceptor(new CmsWebInterceptor());
		String url = CmsConfig.me().getUrl();
		url = StringUtils.isBlank(url) ? "/" : url;
		add(url, IndexController.class);
		url = url.endsWith("/") ? url.substring(0, url.length() - 1) : url;
		add(url + "/blog", BlogController.class);
		add(url + "/news", NewsController.class);
		add(url + "/page", PageController.class);
		add(url + "/qa", QaController.class);
		add(url + "/search", SearchController.class);
		add(url + "/topic", TopicController.class);
	}
}
