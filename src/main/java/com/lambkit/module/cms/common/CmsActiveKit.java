package com.lambkit.module.cms.common;

import com.jfinal.plugin.activerecord.Model;
import com.lambkit.module.cms.core.layer.SortModel;

public class CmsActiveKit {

    /**
     * 用于标识当前 是否选中
     */
    public static final String ACTIVE_FLAG = "isActive";

    /**
     * 标识当前对象（一般情况下是分类、菜单等）
     *
     * @param model
     */
    public static void makeItActive(Model model) {
        model.put(ACTIVE_FLAG, true);

        if (model instanceof SortModel) {
            SortModel parent = ((SortModel) model).getParent();
            //理论上，parent == model 这种情况不可能存在，
            //目前只是为了防止万一哪个兔崽子的代码有问题，从而会出现死循环
            if (parent != null && parent != model) makeItActive((Model) parent);
        }
    }

}
