/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.api;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.service.LambkitService;
import com.lambkit.module.cms.rpc.model.CmsArticle;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public interface CmsArticleService extends LambkitService<CmsArticle> {

	Page<CmsArticle> paginateInNormal(Integer pageNumber, Integer pageSize, String orderBy);
	
	CmsArticle findFirstByAlias(String alias);
	
	Page<CmsArticle> search(Integer pageNumber, Integer pageSize, String keyword);
	/**
	 * 某分类的文章
	 * @param id
	 * @param hasThumbnail
	 * @param orderBy
	 * @param count
	 * @return
	 */
	List<CmsArticle> findListByCategoryId(Long categoryId, String orderBy, Integer count);
	Page<CmsArticle> paginateByCategoryId(Integer pageNumber, Integer pageSize, Long categoryId);
	Page<CmsArticle> paginateByCategoryIdInNormal(Integer pageNumber, Integer pageSize, Long categoryId, String orderBy);
	/**
	 * 下一篇文章
	 * @param articleId
	 * @return
	 */
	CmsArticle findNextById(Long articleId);
	/**
	 * 前一篇文章
	 * @param articleId
	 * @return
	 */
	CmsArticle findPreviousById(Long articleId);
	/**
	 * 相关文章
	 * @param articleId
	 * @param statusNormal
	 * @param count
	 * @return
	 */
	List<CmsArticle> findRelevantListByArticleId(Long articleId, Integer status, Integer count);
	/**
	 * 某标签的文章
	 * @param tagId
	 * @param hasThumbnail
	 * @param orderBy
	 * @param count
	 * @return
	 */
	List<CmsArticle> findListByTagId(Long tagId, String orderBy, Integer count);
	Page<CmsArticle> paginateByTagId(Integer pageNumber, Integer pageSize, Long tagId);
}
