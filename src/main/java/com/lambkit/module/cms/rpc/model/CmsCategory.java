/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.lambkit.module.cms.common.CmsConfig;
import com.lambkit.Lambkit;

import com.lambkit.module.cms.rpc.model.base.BaseCmsCategory;
import com.lambkit.module.cms.rpc.model.sql.CmsCategoryCriteria;
import com.lambkit.module.cms.common.CmsOptions;
import com.lambkit.module.cms.core.layer.SortModel;
import com.lambkit.module.cms.rpc.api.CmsCategoryService;
import com.lambkit.module.cms.rpc.service.impl.CmsCategoryServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-28
 * @version 1.0
 * @since 1.0
 */
public class CmsCategory extends BaseCmsCategory<CmsCategory> implements SortModel {

	private static final long serialVersionUID = 1L;
	
	public static CmsCategoryService service() {
		return ServiceKit.inject(CmsCategoryService.class, CmsCategoryServiceImpl.class);
	}
	
	public static CmsCategoryCriteria sql() {
		return new CmsCategoryCriteria();
	}
	
	public static CmsCategoryCriteria sql(Column column) {
		CmsCategoryCriteria that = new CmsCategoryCriteria();
		that.add(column);
        return that;
    }
	
	public CmsCategory() {
		CmsConfig config = Lambkit.config(CmsConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
	
	private int layerNumber;
    private SortModel parent;
    private List<SortModel> childs;

	@Override
	public boolean isTop() {
        return getPid() != null && getPid() == 0;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return getCategoryId();
	}

	@Override
	public Long getParentId() {
		// TODO Auto-generated method stub
		return getPid();
	}

	@Override
    public void setParent(SortModel parent) {
        this.parent = parent;
    }

    @Override
    public SortModel getParent() {
        return parent;
    }

    @Override
    public void setChilds(List childs) {
        this.childs = childs;
    }

    @Override
    public void addChild(SortModel child) {
        if (childs == null) {
            childs = new ArrayList<>();
        }
        childs.add(child);
    }

    @Override
    public List getChilds() {
        return childs;
    }

    public boolean hasChild() {
        return childs != null && !childs.isEmpty();
    }

    @Override
    public void setLayerNumber(int layerNumber) {
        this.layerNumber = layerNumber;
    }

    @Override
    public int getLayerNumber() {
        return layerNumber;
    }

    public String getLayerString() {
        if (layerNumber == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < layerNumber; i++) {
            if (i == 0)
                sb.append("|—");
            else
                sb.append("—");
        }
        return sb.toString();
    }

    public boolean isMyChild(long id) {
        if (childs == null || childs.isEmpty()) {
            return false;
        }

        return isMyChild(childs, id);
    }

    private boolean isMyChild(List<SortModel> categories, long id) {
        for (SortModel category : categories) {
            if (category.getId() == id) {
                return true;
            }

            if (category.getChilds() != null) {
                boolean isChild = isMyChild(category.getChilds(), id);
                if (isChild) return true;
            }
        }
        return false;
    }
    
    /**
     * 未完成
     * @return
     */
    public String getUrl() {
    	//systemId
    	return JFinal.me().getContextPath() + "/article/category/" + getAlias() + CmsOptions.getAppUrlSuffix();
    }


    public String getHtmlView() {
        return StrKit.isBlank(getStyle()) ? "artlist.html" : "artlist_" + getStyle().trim() + ".html";
    }
}
