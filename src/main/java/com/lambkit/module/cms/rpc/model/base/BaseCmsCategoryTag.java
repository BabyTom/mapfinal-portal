/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseCmsCategoryTag<M extends BaseCmsCategoryTag<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "cms_category_tag";
	}
    
	public java.lang.Long getCategoryTagId() {
		return this.get("category_tag_id");
	}

	public void setCategoryTagId(java.lang.Long categoryTagId) {
		this.set("category_tag_id", categoryTagId);
	}
	public java.lang.Long getCategoryId() {
		return this.get("category_id");
	}

	public void setCategoryId(java.lang.Long categoryId) {
		this.set("category_id", categoryId);
	}
	public java.lang.Long getTagId() {
		return this.get("tag_id");
	}

	public void setTagId(java.lang.Long tagId) {
		this.set("tag_id", tagId);
	}
}
