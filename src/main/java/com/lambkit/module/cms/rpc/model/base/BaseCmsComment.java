/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseCmsComment<M extends BaseCmsComment<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "cms_comment";
	}
    
	public java.lang.Long getCommentId() {
		return this.get("comment_id");
	}

	public void setCommentId(java.lang.Long commentId) {
		this.set("comment_id", commentId);
	}
	public java.lang.Long getPid() {
		return this.get("pid");
	}

	public void setPid(java.lang.Long pid) {
		this.set("pid", pid);
	}
	public java.lang.Long getArticleId() {
		return this.get("article_id");
	}

	public void setArticleId(java.lang.Long articleId) {
		this.set("article_id", articleId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}

	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}

	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}

	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getIp() {
		return this.get("ip");
	}

	public void setIp(java.lang.String ip) {
		this.set("ip", ip);
	}
	public java.lang.String getAgent() {
		return this.get("agent");
	}

	public void setAgent(java.lang.String agent) {
		this.set("agent", agent);
	}
	public java.lang.Long getSystemId() {
		return this.get("system_id");
	}

	public void setSystemId(java.lang.Long systemId) {
		this.set("system_id", systemId);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}

	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
}
