/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseCmsPage<M extends BaseCmsPage<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "cms_page";
	}
    
	public java.lang.Long getPageId() {
		return this.get("page_id");
	}

	public void setPageId(java.lang.Long pageId) {
		this.set("page_id", pageId);
	}
	public java.lang.Integer getPid() {
		return this.get("pid");
	}

	public void setPid(java.lang.Integer pid) {
		this.set("pid", pid);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}

	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getAlias() {
		return this.get("alias");
	}

	public void setAlias(java.lang.String alias) {
		this.set("alias", alias);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}

	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.String getStyle() {
		return this.get("style");
	}

	public void setStyle(java.lang.String style) {
		this.set("style", style);
	}
	public java.lang.String getEditMode() {
		return this.get("edit_mode");
	}

	public void setEditMode(java.lang.String editMode) {
		this.set("edit_mode", editMode);
	}
	public java.lang.String getKeywords() {
		return this.get("keywords");
	}

	public void setKeywords(java.lang.String keywords) {
		this.set("keywords", keywords);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}

	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}

	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.Long getOrders() {
		return this.get("orders");
	}

	public void setOrders(java.lang.Long orders) {
		this.set("orders", orders);
	}
}
