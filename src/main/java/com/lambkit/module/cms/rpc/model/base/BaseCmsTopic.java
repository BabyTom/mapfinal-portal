/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseCmsTopic<M extends BaseCmsTopic<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "cms_topic";
	}
    
	public java.lang.Long getTopicId() {
		return this.get("topic_id");
	}

	public void setTopicId(java.lang.Long topicId) {
		this.set("topic_id", topicId);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}

	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}

	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.String getUrl() {
		return this.get("url");
	}

	public void setUrl(java.lang.String url) {
		this.set("url", url);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}

	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
}
