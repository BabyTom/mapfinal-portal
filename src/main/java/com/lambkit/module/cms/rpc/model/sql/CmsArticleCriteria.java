/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsArticleCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsArticleCriteria create() {
		return new CmsArticleCriteria();
	}
	
	public static CmsArticleCriteria create(Column column) {
		CmsArticleCriteria that = new CmsArticleCriteria();
		that.add(column);
        return that;
    }

    public static CmsArticleCriteria create(String name, Object value) {
        return (CmsArticleCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_article", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsArticleCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsArticleCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsArticleCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsArticleCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsArticleCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsArticleCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsArticleCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsArticleCriteria andArticleIdIsNull() {
		isnull("article_id");
		return this;
	}
	
	public CmsArticleCriteria andArticleIdIsNotNull() {
		notNull("article_id");
		return this;
	}
	
	public CmsArticleCriteria andArticleIdIsEmpty() {
		empty("article_id");
		return this;
	}

	public CmsArticleCriteria andArticleIdIsNotEmpty() {
		notEmpty("article_id");
		return this;
	}
       public CmsArticleCriteria andArticleIdEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.NOT_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdGreaterThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdLessThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_id", value1, value2, ConditionMode.BETWEEN, "articleId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCriteria andArticleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCriteria andArticleIdIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.IN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andArticleIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.NOT_IN, "articleId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCriteria andTopicIdIsNull() {
		isnull("topic_id");
		return this;
	}
	
	public CmsArticleCriteria andTopicIdIsNotNull() {
		notNull("topic_id");
		return this;
	}
	
	public CmsArticleCriteria andTopicIdIsEmpty() {
		empty("topic_id");
		return this;
	}

	public CmsArticleCriteria andTopicIdIsNotEmpty() {
		notEmpty("topic_id");
		return this;
	}
       public CmsArticleCriteria andTopicIdEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdNotEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.NOT_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdGreaterThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdLessThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("topic_id", value1, value2, ConditionMode.BETWEEN, "topicId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleCriteria andTopicIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("topic_id", value1, value2, ConditionMode.NOT_BETWEEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleCriteria andTopicIdIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopicIdNotIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.NOT_IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsArticleCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsArticleCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsArticleCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public CmsArticleCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andAuthorIsNull() {
		isnull("author");
		return this;
	}
	
	public CmsArticleCriteria andAuthorIsNotNull() {
		notNull("author");
		return this;
	}
	
	public CmsArticleCriteria andAuthorIsEmpty() {
		empty("author");
		return this;
	}

	public CmsArticleCriteria andAuthorIsNotEmpty() {
		notEmpty("author");
		return this;
	}
        public CmsArticleCriteria andAuthorLike(java.lang.String value) {
    	   addCriterion("author", value, ConditionMode.FUZZY, "author", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andAuthorNotLike(java.lang.String value) {
          addCriterion("author", value, ConditionMode.NOT_FUZZY, "author", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andAuthorEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorNotEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.NOT_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorGreaterThan(java.lang.String value) {
          addCriterion("author", value, ConditionMode.GREATER_THEN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.GREATER_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorLessThan(java.lang.String value) {
          addCriterion("author", value, ConditionMode.LESS_THEN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorLessThanOrEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.LESS_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("author", value1, value2, ConditionMode.BETWEEN, "author", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andAuthorNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("author", value1, value2, ConditionMode.NOT_BETWEEN, "author", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andAuthorIn(List<java.lang.String> values) {
          addCriterion("author", values, ConditionMode.IN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAuthorNotIn(List<java.lang.String> values) {
          addCriterion("author", values, ConditionMode.NOT_IN, "author", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andFromurlIsNull() {
		isnull("fromurl");
		return this;
	}
	
	public CmsArticleCriteria andFromurlIsNotNull() {
		notNull("fromurl");
		return this;
	}
	
	public CmsArticleCriteria andFromurlIsEmpty() {
		empty("fromurl");
		return this;
	}

	public CmsArticleCriteria andFromurlIsNotEmpty() {
		notEmpty("fromurl");
		return this;
	}
        public CmsArticleCriteria andFromurlLike(java.lang.String value) {
    	   addCriterion("fromurl", value, ConditionMode.FUZZY, "fromurl", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andFromurlNotLike(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.NOT_FUZZY, "fromurl", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andFromurlEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlNotEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.NOT_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlGreaterThan(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.GREATER_THEN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.GREATER_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlLessThan(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.LESS_THEN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.LESS_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fromurl", value1, value2, ConditionMode.BETWEEN, "fromurl", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andFromurlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fromurl", value1, value2, ConditionMode.NOT_BETWEEN, "fromurl", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andFromurlIn(List<java.lang.String> values) {
          addCriterion("fromurl", values, ConditionMode.IN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andFromurlNotIn(List<java.lang.String> values) {
          addCriterion("fromurl", values, ConditionMode.NOT_IN, "fromurl", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andImageIsNull() {
		isnull("image");
		return this;
	}
	
	public CmsArticleCriteria andImageIsNotNull() {
		notNull("image");
		return this;
	}
	
	public CmsArticleCriteria andImageIsEmpty() {
		empty("image");
		return this;
	}

	public CmsArticleCriteria andImageIsNotEmpty() {
		notEmpty("image");
		return this;
	}
        public CmsArticleCriteria andImageLike(java.lang.String value) {
    	   addCriterion("image", value, ConditionMode.FUZZY, "image", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andImageNotLike(java.lang.String value) {
          addCriterion("image", value, ConditionMode.NOT_FUZZY, "image", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andImageEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageNotEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.NOT_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageGreaterThan(java.lang.String value) {
          addCriterion("image", value, ConditionMode.GREATER_THEN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.GREATER_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageLessThan(java.lang.String value) {
          addCriterion("image", value, ConditionMode.LESS_THEN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageLessThanOrEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.LESS_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("image", value1, value2, ConditionMode.BETWEEN, "image", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andImageNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("image", value1, value2, ConditionMode.NOT_BETWEEN, "image", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andImageIn(List<java.lang.String> values) {
          addCriterion("image", values, ConditionMode.IN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andImageNotIn(List<java.lang.String> values) {
          addCriterion("image", values, ConditionMode.NOT_IN, "image", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andKeywordsIsNull() {
		isnull("keywords");
		return this;
	}
	
	public CmsArticleCriteria andKeywordsIsNotNull() {
		notNull("keywords");
		return this;
	}
	
	public CmsArticleCriteria andKeywordsIsEmpty() {
		empty("keywords");
		return this;
	}

	public CmsArticleCriteria andKeywordsIsNotEmpty() {
		notEmpty("keywords");
		return this;
	}
        public CmsArticleCriteria andKeywordsLike(java.lang.String value) {
    	   addCriterion("keywords", value, ConditionMode.FUZZY, "keywords", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andKeywordsNotLike(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_FUZZY, "keywords", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andKeywordsEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsNotEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsGreaterThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsLessThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("keywords", value1, value2, ConditionMode.BETWEEN, "keywords", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andKeywordsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("keywords", value1, value2, ConditionMode.NOT_BETWEEN, "keywords", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andKeywordsIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.IN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andKeywordsNotIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.NOT_IN, "keywords", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsArticleCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsArticleCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsArticleCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsArticleCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsArticleCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsArticleCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsArticleCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public CmsArticleCriteria andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleCriteria andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleCriteria andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleCriteria andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsArticleCriteria andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsArticleCriteria andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsArticleCriteria andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
        public CmsArticleCriteria andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleCriteria andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleCriteria andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andStyleIsNull() {
		isnull("style");
		return this;
	}
	
	public CmsArticleCriteria andStyleIsNotNull() {
		notNull("style");
		return this;
	}
	
	public CmsArticleCriteria andStyleIsEmpty() {
		empty("style");
		return this;
	}

	public CmsArticleCriteria andStyleIsNotEmpty() {
		notEmpty("style");
		return this;
	}
        public CmsArticleCriteria andStyleLike(java.lang.String value) {
    	   addCriterion("style", value, ConditionMode.FUZZY, "style", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andStyleNotLike(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_FUZZY, "style", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andStyleEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleNotEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleGreaterThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleLessThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("style", value1, value2, ConditionMode.BETWEEN, "style", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("style", value1, value2, ConditionMode.NOT_BETWEEN, "style", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andStyleIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.IN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andStyleNotIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.NOT_IN, "style", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andAllowcommentsIsNull() {
		isnull("allowcomments");
		return this;
	}
	
	public CmsArticleCriteria andAllowcommentsIsNotNull() {
		notNull("allowcomments");
		return this;
	}
	
	public CmsArticleCriteria andAllowcommentsIsEmpty() {
		empty("allowcomments");
		return this;
	}

	public CmsArticleCriteria andAllowcommentsIsNotEmpty() {
		notEmpty("allowcomments");
		return this;
	}
       public CmsArticleCriteria andAllowcommentsEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsNotEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.NOT_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsGreaterThan(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.GREATER_THEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.GREATER_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsLessThan(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.LESS_THEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.LESS_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("allowcomments", value1, value2, ConditionMode.BETWEEN, "allowcomments", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleCriteria andAllowcommentsNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("allowcomments", value1, value2, ConditionMode.NOT_BETWEEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleCriteria andAllowcommentsIn(List<java.lang.Integer> values) {
          addCriterion("allowcomments", values, ConditionMode.IN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andAllowcommentsNotIn(List<java.lang.Integer> values) {
          addCriterion("allowcomments", values, ConditionMode.NOT_IN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsArticleCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsArticleCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsArticleCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
       public CmsArticleCriteria andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleCriteria andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleCriteria andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleCriteria andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsArticleCriteria andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsArticleCriteria andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsArticleCriteria andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
        public CmsArticleCriteria andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleCriteria andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleCriteria andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andEditModeIsNull() {
		isnull("edit_mode");
		return this;
	}
	
	public CmsArticleCriteria andEditModeIsNotNull() {
		notNull("edit_mode");
		return this;
	}
	
	public CmsArticleCriteria andEditModeIsEmpty() {
		empty("edit_mode");
		return this;
	}

	public CmsArticleCriteria andEditModeIsNotEmpty() {
		notEmpty("edit_mode");
		return this;
	}
        public CmsArticleCriteria andEditModeLike(java.lang.String value) {
    	   addCriterion("edit_mode", value, ConditionMode.FUZZY, "editMode", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleCriteria andEditModeNotLike(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_FUZZY, "editMode", "java.lang.String", "String");
          return this;
      }
      public CmsArticleCriteria andEditModeEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeNotEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeGreaterThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeLessThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("edit_mode", value1, value2, ConditionMode.BETWEEN, "editMode", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleCriteria andEditModeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("edit_mode", value1, value2, ConditionMode.NOT_BETWEEN, "editMode", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleCriteria andEditModeIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.IN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleCriteria andEditModeNotIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.NOT_IN, "editMode", "java.lang.String", "String");
          return this;
      }
	public CmsArticleCriteria andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsArticleCriteria andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsArticleCriteria andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsArticleCriteria andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public CmsArticleCriteria andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCriteria andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCriteria andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCriteria andReadnumberIsNull() {
		isnull("readnumber");
		return this;
	}
	
	public CmsArticleCriteria andReadnumberIsNotNull() {
		notNull("readnumber");
		return this;
	}
	
	public CmsArticleCriteria andReadnumberIsEmpty() {
		empty("readnumber");
		return this;
	}

	public CmsArticleCriteria andReadnumberIsNotEmpty() {
		notEmpty("readnumber");
		return this;
	}
       public CmsArticleCriteria andReadnumberEqualTo(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.EQUAL, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberNotEqualTo(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.NOT_EQUAL, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberGreaterThan(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.GREATER_THEN, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.GREATER_EQUAL, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberLessThan(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.LESS_THEN, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("readnumber", value, ConditionMode.LESS_EQUAL, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("readnumber", value1, value2, ConditionMode.BETWEEN, "readnumber", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCriteria andReadnumberNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("readnumber", value1, value2, ConditionMode.NOT_BETWEEN, "readnumber", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCriteria andReadnumberIn(List<java.lang.Long> values) {
          addCriterion("readnumber", values, ConditionMode.IN, "readnumber", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andReadnumberNotIn(List<java.lang.Long> values) {
          addCriterion("readnumber", values, ConditionMode.NOT_IN, "readnumber", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCriteria andTopIsNull() {
		isnull("top");
		return this;
	}
	
	public CmsArticleCriteria andTopIsNotNull() {
		notNull("top");
		return this;
	}
	
	public CmsArticleCriteria andTopIsEmpty() {
		empty("top");
		return this;
	}

	public CmsArticleCriteria andTopIsNotEmpty() {
		notEmpty("top");
		return this;
	}
       public CmsArticleCriteria andTopEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopNotEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.NOT_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopGreaterThan(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.GREATER_THEN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.GREATER_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopLessThan(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.LESS_THEN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.LESS_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("top", value1, value2, ConditionMode.BETWEEN, "top", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleCriteria andTopNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("top", value1, value2, ConditionMode.NOT_BETWEEN, "top", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleCriteria andTopIn(List<java.lang.Integer> values) {
          addCriterion("top", values, ConditionMode.IN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleCriteria andTopNotIn(List<java.lang.Integer> values) {
          addCriterion("top", values, ConditionMode.NOT_IN, "top", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleCriteria andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsArticleCriteria andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsArticleCriteria andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsArticleCriteria andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
       public CmsArticleCriteria andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCriteria andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCriteria andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCriteria andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsArticleCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsArticleCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsArticleCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsArticleCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsArticleCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsArticleCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsArticleCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsArticleCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsArticleCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsArticleCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsArticleCriteria andOrdersEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersNotEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersGreaterThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersGreaterThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersLessThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersLessThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.math.BigInteger", "String");
    	  return this;
      }

      public CmsArticleCriteria andOrdersNotBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.math.BigInteger", "String");
          return this;
      }
        
      public CmsArticleCriteria andOrdersIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsArticleCriteria andOrdersNotIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.math.BigInteger", "String");
          return this;
      }
}