/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsArticleTagCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsArticleTagCriteria create() {
		return new CmsArticleTagCriteria();
	}
	
	public static CmsArticleTagCriteria create(Column column) {
		CmsArticleTagCriteria that = new CmsArticleTagCriteria();
		that.add(column);
        return that;
    }

    public static CmsArticleTagCriteria create(String name, Object value) {
        return (CmsArticleTagCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_article_tag", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsArticleTagCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsArticleTagCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleTagCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsArticleTagCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsArticleTagCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsArticleTagCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsArticleTagCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsArticleTagCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsArticleTagCriteria andArticleTagIdIsNull() {
		isnull("article_tag_id");
		return this;
	}
	
	public CmsArticleTagCriteria andArticleTagIdIsNotNull() {
		notNull("article_tag_id");
		return this;
	}
	
	public CmsArticleTagCriteria andArticleTagIdIsEmpty() {
		empty("article_tag_id");
		return this;
	}

	public CmsArticleTagCriteria andArticleTagIdIsNotEmpty() {
		notEmpty("article_tag_id");
		return this;
	}
       public CmsArticleTagCriteria andArticleTagIdEqualTo(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.EQUAL, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.NOT_EQUAL, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdGreaterThan(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.GREATER_THEN, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.GREATER_EQUAL, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdLessThan(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.LESS_THEN, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_tag_id", value, ConditionMode.LESS_EQUAL, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_tag_id", value1, value2, ConditionMode.BETWEEN, "articleTagId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleTagCriteria andArticleTagIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_tag_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleTagId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleTagCriteria andArticleTagIdIn(List<java.lang.Long> values) {
          addCriterion("article_tag_id", values, ConditionMode.IN, "articleTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleTagIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_tag_id", values, ConditionMode.NOT_IN, "articleTagId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleTagCriteria andArticleIdIsNull() {
		isnull("article_id");
		return this;
	}
	
	public CmsArticleTagCriteria andArticleIdIsNotNull() {
		notNull("article_id");
		return this;
	}
	
	public CmsArticleTagCriteria andArticleIdIsEmpty() {
		empty("article_id");
		return this;
	}

	public CmsArticleTagCriteria andArticleIdIsNotEmpty() {
		notEmpty("article_id");
		return this;
	}
       public CmsArticleTagCriteria andArticleIdEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.NOT_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdGreaterThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdLessThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_id", value1, value2, ConditionMode.BETWEEN, "articleId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleTagCriteria andArticleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleTagCriteria andArticleIdIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.IN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andArticleIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.NOT_IN, "articleId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleTagCriteria andTagIdIsNull() {
		isnull("tag_id");
		return this;
	}
	
	public CmsArticleTagCriteria andTagIdIsNotNull() {
		notNull("tag_id");
		return this;
	}
	
	public CmsArticleTagCriteria andTagIdIsEmpty() {
		empty("tag_id");
		return this;
	}

	public CmsArticleTagCriteria andTagIdIsNotEmpty() {
		notEmpty("tag_id");
		return this;
	}
       public CmsArticleTagCriteria andTagIdEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdNotEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.NOT_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdGreaterThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdLessThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("tag_id", value1, value2, ConditionMode.BETWEEN, "tagId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleTagCriteria andTagIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("tag_id", value1, value2, ConditionMode.NOT_BETWEEN, "tagId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleTagCriteria andTagIdIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.IN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleTagCriteria andTagIdNotIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.NOT_IN, "tagId", "java.lang.Long", "Float");
          return this;
      }
}