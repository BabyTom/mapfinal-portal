/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsCommentCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsCommentCriteria create() {
		return new CmsCommentCriteria();
	}
	
	public static CmsCommentCriteria create(Column column) {
		CmsCommentCriteria that = new CmsCommentCriteria();
		that.add(column);
        return that;
    }

    public static CmsCommentCriteria create(String name, Object value) {
        return (CmsCommentCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_comment", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsCommentCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsCommentCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsCommentCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsCommentCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsCommentCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsCommentCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsCommentCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsCommentCriteria andCommentIdIsNull() {
		isnull("comment_id");
		return this;
	}
	
	public CmsCommentCriteria andCommentIdIsNotNull() {
		notNull("comment_id");
		return this;
	}
	
	public CmsCommentCriteria andCommentIdIsEmpty() {
		empty("comment_id");
		return this;
	}

	public CmsCommentCriteria andCommentIdIsNotEmpty() {
		notEmpty("comment_id");
		return this;
	}
       public CmsCommentCriteria andCommentIdEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdNotEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.NOT_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdGreaterThan(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.GREATER_THEN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.GREATER_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdLessThan(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.LESS_THEN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.LESS_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("comment_id", value1, value2, ConditionMode.BETWEEN, "commentId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentCriteria andCommentIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("comment_id", value1, value2, ConditionMode.NOT_BETWEEN, "commentId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentCriteria andCommentIdIn(List<java.lang.Long> values) {
          addCriterion("comment_id", values, ConditionMode.IN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andCommentIdNotIn(List<java.lang.Long> values) {
          addCriterion("comment_id", values, ConditionMode.NOT_IN, "commentId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentCriteria andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsCommentCriteria andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsCommentCriteria andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsCommentCriteria andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
       public CmsCommentCriteria andPidEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidNotEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidGreaterThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidLessThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentCriteria andPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentCriteria andPidIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andPidNotIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentCriteria andArticleIdIsNull() {
		isnull("article_id");
		return this;
	}
	
	public CmsCommentCriteria andArticleIdIsNotNull() {
		notNull("article_id");
		return this;
	}
	
	public CmsCommentCriteria andArticleIdIsEmpty() {
		empty("article_id");
		return this;
	}

	public CmsCommentCriteria andArticleIdIsNotEmpty() {
		notEmpty("article_id");
		return this;
	}
       public CmsCommentCriteria andArticleIdEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.NOT_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdGreaterThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdLessThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_id", value1, value2, ConditionMode.BETWEEN, "articleId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentCriteria andArticleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentCriteria andArticleIdIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.IN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andArticleIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.NOT_IN, "articleId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentCriteria andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsCommentCriteria andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsCommentCriteria andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsCommentCriteria andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public CmsCommentCriteria andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentCriteria andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentCriteria andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentCriteria andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsCommentCriteria andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsCommentCriteria andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsCommentCriteria andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
        public CmsCommentCriteria andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCommentCriteria andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "Float");
          return this;
      }
      public CmsCommentCriteria andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentCriteria andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentCriteria andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsCommentCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsCommentCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsCommentCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsCommentCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
       public CmsCommentCriteria andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCommentCriteria andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCommentCriteria andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentCriteria andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCommentCriteria andIpIsNull() {
		isnull("ip");
		return this;
	}
	
	public CmsCommentCriteria andIpIsNotNull() {
		notNull("ip");
		return this;
	}
	
	public CmsCommentCriteria andIpIsEmpty() {
		empty("ip");
		return this;
	}

	public CmsCommentCriteria andIpIsNotEmpty() {
		notEmpty("ip");
		return this;
	}
        public CmsCommentCriteria andIpLike(java.lang.String value) {
    	   addCriterion("ip", value, ConditionMode.FUZZY, "ip", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCommentCriteria andIpNotLike(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_FUZZY, "ip", "java.lang.String", "Float");
          return this;
      }
      public CmsCommentCriteria andIpEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpNotEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpGreaterThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpLessThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ip", value1, value2, ConditionMode.BETWEEN, "ip", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentCriteria andIpNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ip", value1, value2, ConditionMode.NOT_BETWEEN, "ip", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentCriteria andIpIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.IN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andIpNotIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.NOT_IN, "ip", "java.lang.String", "String");
          return this;
      }
	public CmsCommentCriteria andAgentIsNull() {
		isnull("agent");
		return this;
	}
	
	public CmsCommentCriteria andAgentIsNotNull() {
		notNull("agent");
		return this;
	}
	
	public CmsCommentCriteria andAgentIsEmpty() {
		empty("agent");
		return this;
	}

	public CmsCommentCriteria andAgentIsNotEmpty() {
		notEmpty("agent");
		return this;
	}
        public CmsCommentCriteria andAgentLike(java.lang.String value) {
    	   addCriterion("agent", value, ConditionMode.FUZZY, "agent", "java.lang.String", "String");
    	   return this;
      }

      public CmsCommentCriteria andAgentNotLike(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.NOT_FUZZY, "agent", "java.lang.String", "String");
          return this;
      }
      public CmsCommentCriteria andAgentEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentNotEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.NOT_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentGreaterThan(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.GREATER_THEN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.GREATER_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentLessThan(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.LESS_THEN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.LESS_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("agent", value1, value2, ConditionMode.BETWEEN, "agent", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentCriteria andAgentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("agent", value1, value2, ConditionMode.NOT_BETWEEN, "agent", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentCriteria andAgentIn(List<java.lang.String> values) {
          addCriterion("agent", values, ConditionMode.IN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentCriteria andAgentNotIn(List<java.lang.String> values) {
          addCriterion("agent", values, ConditionMode.NOT_IN, "agent", "java.lang.String", "String");
          return this;
      }
	public CmsCommentCriteria andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsCommentCriteria andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsCommentCriteria andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsCommentCriteria andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
       public CmsCommentCriteria andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentCriteria andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentCriteria andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentCriteria andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsCommentCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsCommentCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsCommentCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsCommentCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsCommentCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsCommentCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
}