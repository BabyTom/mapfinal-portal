/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsPageCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsPageCriteria create() {
		return new CmsPageCriteria();
	}
	
	public static CmsPageCriteria create(Column column) {
		CmsPageCriteria that = new CmsPageCriteria();
		that.add(column);
        return that;
    }

    public static CmsPageCriteria create(String name, Object value) {
        return (CmsPageCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_page", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsPageCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsPageCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsPageCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsPageCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsPageCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsPageCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsPageCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsPageCriteria andPageIdIsNull() {
		isnull("page_id");
		return this;
	}
	
	public CmsPageCriteria andPageIdIsNotNull() {
		notNull("page_id");
		return this;
	}
	
	public CmsPageCriteria andPageIdIsEmpty() {
		empty("page_id");
		return this;
	}

	public CmsPageCriteria andPageIdIsNotEmpty() {
		notEmpty("page_id");
		return this;
	}
       public CmsPageCriteria andPageIdEqualTo(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.EQUAL, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdNotEqualTo(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.NOT_EQUAL, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdGreaterThan(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.GREATER_THEN, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.GREATER_EQUAL, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdLessThan(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.LESS_THEN, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("page_id", value, ConditionMode.LESS_EQUAL, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("page_id", value1, value2, ConditionMode.BETWEEN, "pageId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsPageCriteria andPageIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("page_id", value1, value2, ConditionMode.NOT_BETWEEN, "pageId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsPageCriteria andPageIdIn(List<java.lang.Long> values) {
          addCriterion("page_id", values, ConditionMode.IN, "pageId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andPageIdNotIn(List<java.lang.Long> values) {
          addCriterion("page_id", values, ConditionMode.NOT_IN, "pageId", "java.lang.Long", "Float");
          return this;
      }
	public CmsPageCriteria andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsPageCriteria andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsPageCriteria andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsPageCriteria andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
       public CmsPageCriteria andPidEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidNotEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidGreaterThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidLessThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsPageCriteria andPidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsPageCriteria andPidIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageCriteria andPidNotIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Integer", "Float");
          return this;
      }
	public CmsPageCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsPageCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsPageCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsPageCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public CmsPageCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsPageCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsPageCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsPageCriteria andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsPageCriteria andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsPageCriteria andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
        public CmsPageCriteria andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsPageCriteria andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsPageCriteria andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsPageCriteria andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
        public CmsPageCriteria andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andStyleIsNull() {
		isnull("style");
		return this;
	}
	
	public CmsPageCriteria andStyleIsNotNull() {
		notNull("style");
		return this;
	}
	
	public CmsPageCriteria andStyleIsEmpty() {
		empty("style");
		return this;
	}

	public CmsPageCriteria andStyleIsNotEmpty() {
		notEmpty("style");
		return this;
	}
        public CmsPageCriteria andStyleLike(java.lang.String value) {
    	   addCriterion("style", value, ConditionMode.FUZZY, "style", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andStyleNotLike(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_FUZZY, "style", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andStyleEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleNotEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleGreaterThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleLessThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("style", value1, value2, ConditionMode.BETWEEN, "style", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("style", value1, value2, ConditionMode.NOT_BETWEEN, "style", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andStyleIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.IN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andStyleNotIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.NOT_IN, "style", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andEditModeIsNull() {
		isnull("edit_mode");
		return this;
	}
	
	public CmsPageCriteria andEditModeIsNotNull() {
		notNull("edit_mode");
		return this;
	}
	
	public CmsPageCriteria andEditModeIsEmpty() {
		empty("edit_mode");
		return this;
	}

	public CmsPageCriteria andEditModeIsNotEmpty() {
		notEmpty("edit_mode");
		return this;
	}
        public CmsPageCriteria andEditModeLike(java.lang.String value) {
    	   addCriterion("edit_mode", value, ConditionMode.FUZZY, "editMode", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andEditModeNotLike(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_FUZZY, "editMode", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andEditModeEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeNotEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeGreaterThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeLessThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("edit_mode", value1, value2, ConditionMode.BETWEEN, "editMode", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andEditModeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("edit_mode", value1, value2, ConditionMode.NOT_BETWEEN, "editMode", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andEditModeIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.IN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andEditModeNotIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.NOT_IN, "editMode", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andKeywordsIsNull() {
		isnull("keywords");
		return this;
	}
	
	public CmsPageCriteria andKeywordsIsNotNull() {
		notNull("keywords");
		return this;
	}
	
	public CmsPageCriteria andKeywordsIsEmpty() {
		empty("keywords");
		return this;
	}

	public CmsPageCriteria andKeywordsIsNotEmpty() {
		notEmpty("keywords");
		return this;
	}
        public CmsPageCriteria andKeywordsLike(java.lang.String value) {
    	   addCriterion("keywords", value, ConditionMode.FUZZY, "keywords", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andKeywordsNotLike(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_FUZZY, "keywords", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andKeywordsEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsNotEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsGreaterThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsLessThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("keywords", value1, value2, ConditionMode.BETWEEN, "keywords", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andKeywordsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("keywords", value1, value2, ConditionMode.NOT_BETWEEN, "keywords", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andKeywordsIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.IN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andKeywordsNotIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.NOT_IN, "keywords", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsPageCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsPageCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsPageCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsPageCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsPageCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsPageCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsPageCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsPageCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsPageCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsPageCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsPageCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsPageCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsPageCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsPageCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsPageCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsPageCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsPageCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsPageCriteria andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsPageCriteria andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsPageCriteria andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageCriteria andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}