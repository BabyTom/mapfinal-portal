/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsSettingCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsSettingCriteria create() {
		return new CmsSettingCriteria();
	}
	
	public static CmsSettingCriteria create(Column column) {
		CmsSettingCriteria that = new CmsSettingCriteria();
		that.add(column);
        return that;
    }

    public static CmsSettingCriteria create(String name, Object value) {
        return (CmsSettingCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_setting", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsSettingCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsSettingCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsSettingCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsSettingCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsSettingCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsSettingCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsSettingCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsSettingCriteria andSettingIdIsNull() {
		isnull("setting_id");
		return this;
	}
	
	public CmsSettingCriteria andSettingIdIsNotNull() {
		notNull("setting_id");
		return this;
	}
	
	public CmsSettingCriteria andSettingIdIsEmpty() {
		empty("setting_id");
		return this;
	}

	public CmsSettingCriteria andSettingIdIsNotEmpty() {
		notEmpty("setting_id");
		return this;
	}
       public CmsSettingCriteria andSettingIdEqualTo(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.EQUAL, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdNotEqualTo(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.NOT_EQUAL, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdGreaterThan(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.GREATER_THEN, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.GREATER_EQUAL, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdLessThan(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.LESS_THEN, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("setting_id", value, ConditionMode.LESS_EQUAL, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("setting_id", value1, value2, ConditionMode.BETWEEN, "settingId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsSettingCriteria andSettingIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("setting_id", value1, value2, ConditionMode.NOT_BETWEEN, "settingId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsSettingCriteria andSettingIdIn(List<java.lang.Long> values) {
          addCriterion("setting_id", values, ConditionMode.IN, "settingId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSettingCriteria andSettingIdNotIn(List<java.lang.Long> values) {
          addCriterion("setting_id", values, ConditionMode.NOT_IN, "settingId", "java.lang.Long", "Float");
          return this;
      }
	public CmsSettingCriteria andSettingKeyIsNull() {
		isnull("setting_key");
		return this;
	}
	
	public CmsSettingCriteria andSettingKeyIsNotNull() {
		notNull("setting_key");
		return this;
	}
	
	public CmsSettingCriteria andSettingKeyIsEmpty() {
		empty("setting_key");
		return this;
	}

	public CmsSettingCriteria andSettingKeyIsNotEmpty() {
		notEmpty("setting_key");
		return this;
	}
        public CmsSettingCriteria andSettingKeyLike(java.lang.String value) {
    	   addCriterion("setting_key", value, ConditionMode.FUZZY, "settingKey", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSettingCriteria andSettingKeyNotLike(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.NOT_FUZZY, "settingKey", "java.lang.String", "Float");
          return this;
      }
      public CmsSettingCriteria andSettingKeyEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyNotEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.NOT_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyGreaterThan(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.GREATER_THEN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.GREATER_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyLessThan(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.LESS_THEN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.LESS_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("setting_key", value1, value2, ConditionMode.BETWEEN, "settingKey", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingCriteria andSettingKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("setting_key", value1, value2, ConditionMode.NOT_BETWEEN, "settingKey", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingCriteria andSettingKeyIn(List<java.lang.String> values) {
          addCriterion("setting_key", values, ConditionMode.IN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingKeyNotIn(List<java.lang.String> values) {
          addCriterion("setting_key", values, ConditionMode.NOT_IN, "settingKey", "java.lang.String", "String");
          return this;
      }
	public CmsSettingCriteria andSettingValueIsNull() {
		isnull("setting_value");
		return this;
	}
	
	public CmsSettingCriteria andSettingValueIsNotNull() {
		notNull("setting_value");
		return this;
	}
	
	public CmsSettingCriteria andSettingValueIsEmpty() {
		empty("setting_value");
		return this;
	}

	public CmsSettingCriteria andSettingValueIsNotEmpty() {
		notEmpty("setting_value");
		return this;
	}
        public CmsSettingCriteria andSettingValueLike(java.lang.String value) {
    	   addCriterion("setting_value", value, ConditionMode.FUZZY, "settingValue", "java.lang.String", "String");
    	   return this;
      }

      public CmsSettingCriteria andSettingValueNotLike(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.NOT_FUZZY, "settingValue", "java.lang.String", "String");
          return this;
      }
      public CmsSettingCriteria andSettingValueEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueNotEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.NOT_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueGreaterThan(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.GREATER_THEN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.GREATER_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueLessThan(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.LESS_THEN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.LESS_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("setting_value", value1, value2, ConditionMode.BETWEEN, "settingValue", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingCriteria andSettingValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("setting_value", value1, value2, ConditionMode.NOT_BETWEEN, "settingValue", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingCriteria andSettingValueIn(List<java.lang.String> values) {
          addCriterion("setting_value", values, ConditionMode.IN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingCriteria andSettingValueNotIn(List<java.lang.String> values) {
          addCriterion("setting_value", values, ConditionMode.NOT_IN, "settingValue", "java.lang.String", "String");
          return this;
      }
}