/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsTagCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsTagCriteria create() {
		return new CmsTagCriteria();
	}
	
	public static CmsTagCriteria create(Column column) {
		CmsTagCriteria that = new CmsTagCriteria();
		that.add(column);
        return that;
    }

    public static CmsTagCriteria create(String name, Object value) {
        return (CmsTagCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_tag", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsTagCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsTagCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTagCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsTagCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsTagCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsTagCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsTagCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsTagCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsTagCriteria andTagIdIsNull() {
		isnull("tag_id");
		return this;
	}
	
	public CmsTagCriteria andTagIdIsNotNull() {
		notNull("tag_id");
		return this;
	}
	
	public CmsTagCriteria andTagIdIsEmpty() {
		empty("tag_id");
		return this;
	}

	public CmsTagCriteria andTagIdIsNotEmpty() {
		notEmpty("tag_id");
		return this;
	}
       public CmsTagCriteria andTagIdEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdNotEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.NOT_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdGreaterThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdLessThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("tag_id", value1, value2, ConditionMode.BETWEEN, "tagId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsTagCriteria andTagIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("tag_id", value1, value2, ConditionMode.NOT_BETWEEN, "tagId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsTagCriteria andTagIdIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.IN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andTagIdNotIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.NOT_IN, "tagId", "java.lang.Long", "Float");
          return this;
      }
	public CmsTagCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsTagCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsTagCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsTagCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public CmsTagCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsTagCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsTagCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsTagCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsTagCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsTagCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsTagCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsTagCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsTagCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsTagCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsTagCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsTagCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsTagCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsTagCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsTagCriteria andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public CmsTagCriteria andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public CmsTagCriteria andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public CmsTagCriteria andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
        public CmsTagCriteria andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "String");
    	   return this;
      }

      public CmsTagCriteria andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "String");
          return this;
      }
      public CmsTagCriteria andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public CmsTagCriteria andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public CmsTagCriteria andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public CmsTagCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsTagCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsTagCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsTagCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public CmsTagCriteria andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsTagCriteria andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsTagCriteria andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTagCriteria andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public CmsTagCriteria andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsTagCriteria andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsTagCriteria andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsTagCriteria andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
        public CmsTagCriteria andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "Float");
    	   return this;
      }

      public CmsTagCriteria andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "Float");
          return this;
      }
      public CmsTagCriteria andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsTagCriteria andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsTagCriteria andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsTagCriteria andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsTagCriteria andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsTagCriteria andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsTagCriteria andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsTagCriteria andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
       public CmsTagCriteria andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsTagCriteria andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsTagCriteria andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTagCriteria andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsTagCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsTagCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsTagCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsTagCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsTagCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsTagCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsTagCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTagCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsTagCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsTagCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsTagCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsTagCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsTagCriteria andOrdersEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersNotEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersGreaterThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersGreaterThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersLessThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersLessThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.math.BigInteger", "String");
    	  return this;
      }

      public CmsTagCriteria andOrdersNotBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.math.BigInteger", "String");
          return this;
      }
        
      public CmsTagCriteria andOrdersIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsTagCriteria andOrdersNotIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.math.BigInteger", "String");
          return this;
      }
}