/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsTopicCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsTopicCriteria create() {
		return new CmsTopicCriteria();
	}
	
	public static CmsTopicCriteria create(Column column) {
		CmsTopicCriteria that = new CmsTopicCriteria();
		that.add(column);
        return that;
    }

    public static CmsTopicCriteria create(String name, Object value) {
        return (CmsTopicCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_topic", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsTopicCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsTopicCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsTopicCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsTopicCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsTopicCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsTopicCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsTopicCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsTopicCriteria andTopicIdIsNull() {
		isnull("topic_id");
		return this;
	}
	
	public CmsTopicCriteria andTopicIdIsNotNull() {
		notNull("topic_id");
		return this;
	}
	
	public CmsTopicCriteria andTopicIdIsEmpty() {
		empty("topic_id");
		return this;
	}

	public CmsTopicCriteria andTopicIdIsNotEmpty() {
		notEmpty("topic_id");
		return this;
	}
       public CmsTopicCriteria andTopicIdEqualTo(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.EQUAL, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdNotEqualTo(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.NOT_EQUAL, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdGreaterThan(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_THEN, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_EQUAL, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdLessThan(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.LESS_THEN, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("topic_id", value, ConditionMode.LESS_EQUAL, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("topic_id", value1, value2, ConditionMode.BETWEEN, "topicId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsTopicCriteria andTopicIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("topic_id", value1, value2, ConditionMode.NOT_BETWEEN, "topicId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsTopicCriteria andTopicIdIn(List<java.lang.Long> values) {
          addCriterion("topic_id", values, ConditionMode.IN, "topicId", "java.lang.Long", "Float");
          return this;
      }

      public CmsTopicCriteria andTopicIdNotIn(List<java.lang.Long> values) {
          addCriterion("topic_id", values, ConditionMode.NOT_IN, "topicId", "java.lang.Long", "Float");
          return this;
      }
	public CmsTopicCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsTopicCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsTopicCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsTopicCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public CmsTopicCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsTopicCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsTopicCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsTopicCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsTopicCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsTopicCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsTopicCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsTopicCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsTopicCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsTopicCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsTopicCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public CmsTopicCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public CmsTopicCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public CmsTopicCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public CmsTopicCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public CmsTopicCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public CmsTopicCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public CmsTopicCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsTopicCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsTopicCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsTopicCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsTopicCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsTopicCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsTopicCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
}