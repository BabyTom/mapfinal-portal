package com.lambkit.module.cms.rpc.search.impl;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.search.ArticleSearcher;

public class DbSearcher implements ArticleSearcher {

	@Override
    public void addArticle(CmsArticle article) {
        // do noting
    }

    @Override
    public void deleteArticle(Object id) {
        // do noting
    }

    @Override
    public void updateArticle(CmsArticle article) {
        // do noting
    }

    @Override
    public Page<CmsArticle> search(String keyword, int pageNum, int pageSize) {
        return CmsArticle.service().search(pageNum, pageSize, keyword);
    }
}
