/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.service.impl;

import java.util.List;

import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.common.service.LambkitModelServiceImpl;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.rpc.api.CmsCategoryService;
import com.lambkit.module.cms.rpc.model.CmsCategory;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public class CmsCategoryServiceImpl extends LambkitModelServiceImpl<CmsCategory> implements CmsCategoryService {
	
	private CmsCategory DAO = null;
	
	public CmsCategory dao() {
		if(DAO==null) {
			DAO = AopKit.singleton(CmsCategory.class);
		}
		return DAO;
	}

	@Override
	public List<CmsCategory> findListBySystemId(Long systemId) {
		Example example = CmsCategory.sql().andSystemIdEqualTo(systemId).example();
		return find(example);
	}

	@Override
	public List<CmsCategory> findCategoryListByArticleId(Long articleId) {
		SqlPara sqlPara = dao().getSqlPara("cms.findCategoryListByArticleId", articleId);
		return dao().find(sqlPara);
	}

	@Override
	public CmsCategory findFirstByFlag(String flag) {
		Example example = CmsCategory.sql().andFlagEqualTo(flag).example();
		return findFirst(example);
	}
}
