/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.service.impl;

import com.lambkit.common.service.LambkitModelServiceImpl;
import com.lambkit.common.util.StringUtils;
import com.lambkit.core.aop.AopKit;

import com.lambkit.module.cms.rpc.api.CmsSettingService;
import com.lambkit.module.cms.rpc.model.CmsSetting;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public class CmsSettingServiceImpl extends LambkitModelServiceImpl<CmsSetting> implements CmsSettingService {
	
	private CmsSetting DAO = null;
	
	public CmsSetting dao() {
		if(DAO==null) {
			DAO = AopKit.singleton(CmsSetting.class);
		}
		return DAO;
	}

	@Override
	public String get(String key) {
		// TODO Auto-generated method stub
		CmsSetting setting = findFirst(CmsSetting.sql().andSettingKeyEqualTo(key).example());
		return setting!=null ? setting.getSettingValue() : null;
	}

	@Override
	public void remove(String key) {
		// TODO Auto-generated method stub
		delete(CmsSetting.sql().andSettingKeyEqualTo(key).example());
	}

	@Override
	public void put(String key, String value) {
		// TODO Auto-generated method stub
		if(StringUtils.isBlank(key) || StringUtils.isBlank(value)) {
			return;
		} 
		if(StringUtils.isBlank(value)) {
			remove(key);
			return;
		} 
		CmsSetting setting = findFirst(CmsSetting.sql().andSettingKeyEqualTo(key).example());
		if(setting==null) {
			setting = new CmsSetting();
		}
		setting.setSettingKey(key);
		setting.setSettingValue(value);
		setting.saveOrUpdate();
	}
}
