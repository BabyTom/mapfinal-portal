package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsCategory;
import com.lambkit.module.cms.web.validator.CmsCategoryValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

import java.math.BigInteger;

/**
 * 类目控制器
 */
//@Controller
@Api(tag = "类目管理", description = "类目管理")
//@RequestMapping("/manage/category")
public class CmsCategoryController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsCategoryController.class);
	
	@ApiOperation(tag = "类目首页")
	@RequiresPermissions("cms:category:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/category/index.html");
	}

	@ApiOperation(tag = "类目列表")
	@RequiresPermissions("cms:category:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsCategoryExample = CmsCategory.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsCategoryExample.setOrderBy(sort + " " + order);
		}
		Page<CmsCategory> categorys = CmsCategory.service().paginate(cmsCategoryExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, categorys));
	}

	@ApiOperation(tag = "新增类目")
	@RequiresPermissions("cms:category:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsCategoryValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/category/create.html");
		} else {
			CmsCategory cmsCategory = getModel(CmsCategory.class, "category");
			long time = System.currentTimeMillis();
			cmsCategory.setCtime(DateTimeUtils.parse(time));
			cmsCategory.setOrders(BigInteger.valueOf(time));
			boolean flag = cmsCategory.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除类目")
	@RequiresPermissions("cms:category:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsCategory.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改类目")
	@RequiresPermissions("cms:category:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsCategoryValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsCategory category = CmsCategory.service().findById(id);
			setAttr("category", category);
			render(AdminManager.me().getTemplatePath() + "/cms/category/update.html");
		} else {
			CmsCategory cmsCategory = getModel(CmsCategory.class, "category");
			boolean flag = false;
			if(cmsCategory.getCategoryId()!=null) {
				flag = cmsCategory.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}