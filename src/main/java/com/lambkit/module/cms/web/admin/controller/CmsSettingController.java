package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.StringUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsSetting;
import com.lambkit.module.cms.web.validator.CmsSettingValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

/**
 * 设置控制器
 */
//@Controller
@Api(tag = "设置管理", description = "设置管理")
//@RequestMapping("/manage/setting")
public class CmsSettingController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsSettingController.class);
	
	@ApiOperation(tag = "设置首页")
	@RequiresPermissions("cms:setting:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/setting/index.html");
	}

	@ApiOperation(tag = "设置列表")
	@RequiresPermissions("cms:setting:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmssettingExample = CmsSetting.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmssettingExample.setOrderBy(StringUtils.humpToLine(sort) + " " + order);
		}
		Page<CmsSetting> settings = CmsSetting.service().paginate(cmssettingExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, settings));
	}

	@ApiOperation(tag = "新增设置")
	@RequiresPermissions("cms:setting:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsSettingValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/setting/create.html");
		} else {
			CmsSetting cmsSetting = getModel(CmsSetting.class, "setting");
			boolean flag = cmsSetting.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除设置")
	@RequiresPermissions("cms:setting:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsSetting.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改设置")
	@RequiresPermissions("cms:setting:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsSettingValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsSetting setting = CmsSetting.service().findById(id);
			setAttr("setting", setting);
			render(AdminManager.me().getTemplatePath() + "/cms/setting/update.html");
		} else {
			CmsSetting cmsSetting = getModel(CmsSetting.class, "setting");
			boolean flag = false;
			if(cmsSetting.getSettingId()!=null) {
				flag = cmsSetting.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}
