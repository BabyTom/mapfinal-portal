package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsTopic;
import com.lambkit.module.cms.web.validator.CmsTopicValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

/**
 * 专题控制器
 */
//@Controller
@Api(tag = "专题管理", description = "专题管理")
//@RequestMapping("/manage/topic")
public class CmsTopicController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsTopicController.class);
	
	@ApiOperation(tag = "专题首页")
	@RequiresPermissions("cms:topic:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/topic/index.html");
	}

	@ApiOperation(tag = "专题列表")
	@RequiresPermissions("cms:topic:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsTopicExample = CmsTopic.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsTopicExample.setOrderBy(sort + " " + order);
		}
		Page<CmsTopic> topics = CmsTopic.service().paginate(cmsTopicExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, topics));
	}

	@ApiOperation(tag = "新增专题")
	@RequiresPermissions("cms:topic:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsTopicValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/topic/create.html");
		} else {
			CmsTopic cmsTopic = getModel(CmsTopic.class, "topic");
			long time = System.currentTimeMillis();
			cmsTopic.setCtime(DateTimeUtils.parse(time));
			boolean flag = cmsTopic.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除专题")
	@RequiresPermissions("cms:topic:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsTopic.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改专题")
	@RequiresPermissions("cms:topic:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsTopicValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsTopic topic = CmsTopic.service().findById(id);
			setAttr("topic", topic);
			render(AdminManager.me().getTemplatePath() + "/cms/topic/update.html");
		} else {
			CmsTopic cmsTopic = getModel(CmsTopic.class, "topic");
			boolean flag = false;
			if(cmsTopic.getTopicId()!=null) {
				flag = cmsTopic.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}