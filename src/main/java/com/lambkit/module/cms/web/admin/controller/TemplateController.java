package com.lambkit.module.cms.web.admin.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.core.template.Template;
import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.web.controller.LambkitController;

/**
 * 模板控制器
 */
//@Controller
@Api(tag = "模板管理", description = "模板管理")
//@RequestMapping("/manage/template")
public class TemplateController extends LambkitController {

	@ApiOperation(tag = "模板首页")
	@RequiresPermissions("cms:template:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/article/index.html");
	}
	
	@ApiOperation(tag = "模板列表")
	@RequiresPermissions("cms:template:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		Template currentTemplate = TemplateManager.me().getCurrentTemplate();
		List<Template> templates = TemplateManager.me().getInstalledTemplates();
		Kv data = Kv.by("templates", templates).set("current", currentTemplate);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, data));
	}

	@ApiOperation(tag = "当前模板")
	@RequiresPermissions("cms:template:read")
	public void current() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, TemplateManager.me().getCurrentTemplate()));
		} else {
			String templateId = getPara(0, getPara("templateId"));
			if(StrKit.notBlank(templateId)) {
				TemplateManager.me().setCurrentTemplate(templateId);
				renderJson(new CmsResult(CmsResultConstant.SUCCESS, TemplateManager.me().getCurrentTemplate()));
			} else {
				renderJson(new CmsResult(CmsResultConstant.FAILED, TemplateManager.me().getCurrentTemplate()));
			}
		}
	}
}
