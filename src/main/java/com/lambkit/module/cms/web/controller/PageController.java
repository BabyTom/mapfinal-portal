package com.lambkit.module.cms.web.controller;

import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.CmsPage;
import com.lambkit.web.controller.LambkitController;

import com.jfinal.log.Log;


/**
 * 单页控制器
 */
//@RequestMapping(value = "/page")
public class PageController extends LambkitController {

    private static final Log LOG = Log.getLog(PageController.class);

    public void index() {
    	String alias = getPara(0);
    	Example cmsPageExample = CmsPage.sql().andAliasEqualTo(alias).example();
    	CmsPage page = CmsPage.service().findFirst(cmsPageExample);
        setAttr("page", page);
        render(TemplateManager.me().getCurrentWebPath() + "/page/index.html");
    }

}