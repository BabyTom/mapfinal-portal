package com.lambkit.module.cms.web.controller;

import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.common.util.RequestUtils;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.*;
import com.lambkit.web.controller.LambkitController;
import org.apache.commons.lang.StringUtils;
import com.jfinal.log.Log;


import java.util.List;

/**
 * 问答首页控制器
 */
//@RequestMapping(value = "/qa")
public class QaController extends LambkitController {

    private static final Log LOG = Log.getLog(QaController.class);
    private static String CODE = "qa";
    private static Long USERID = 1L;

    /**
     * 首页
     * @param page
     * @param sort
     * @param order
     * @param request
     * @param model
     * @return
     */
    //@RequestMapping(value = "", method = RequestMethod.GET)
    public void index() {
		int page = getParaToInt(0, 1);
		String sort = getPara(1, "orders");
		String order = getPara(2, "desc");
        // 系统id
        Example cmsSystemExample = CmsSystem.sql().andCodeEqualTo(CODE).example();
        CmsSystem system = CmsSystem.service().findFirst(cmsSystemExample);
        setAttr("system", system);
        // 该系统类目
        Example cmsCategoryExample = CmsCategory.sql().andSystemIdEqualTo(system.getSystemId()).example();
        cmsCategoryExample.setOrderBy("orders asc");
        List<CmsCategory> categories = CmsCategory.service().find(cmsCategoryExample);
        setAttr("categories", categories);
        // 该系统标签
        Example cmsTagExample = CmsTag.sql().andSystemIdEqualTo(system.getSystemId()).example();
        cmsTagExample.setOrderBy("orders asc");
        List<CmsTag> tags = CmsTag.service().find(cmsTagExample);
        setAttr("tags", tags);
        // 该系统文章列表
        int rows = 10;
        Example cmsArticleExample = CmsArticle.sql().andStatusEqualTo(1)
                .andSystemIdEqualTo(system.getSystemId()).example();
        if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
            cmsArticleExample.setOrderBy(sort + " " + order);
        }
        setAttr("articles", CmsArticle.service().paginate(page, rows, cmsArticleExample));
        render(TemplateManager.me().getCurrentWebPath() + "/qa/index.html");
    }

    /**
     * 类目页
     * @param alias
     * @param page
     * @param request
     * @param model
     * @return
     */
    //@RequestMapping(value = "/category/{alias}", method = RequestMethod.GET)
    public void category() {
    	String alias = getPara(0);
		int page = getParaToInt(1, 1);
        // 系统id
        Example cmsSystemExample = CmsSystem.sql().andCodeEqualTo(CODE).example();
        CmsSystem system = CmsSystem.service().findFirst(cmsSystemExample);
        setAttr("system", system);
        // 当前类目
       Example cmsCategoryExample =  CmsCategory.sql().andSystemIdEqualTo(system.getSystemId())
                .andAliasEqualTo(alias).example();
        CmsCategory category =  CmsCategory.service().findFirst(cmsCategoryExample);
        setAttr("category", category);
        // 该类目文章列表
        int rows = 10;
        setAttr("articles", CmsArticle.service().paginateByCategoryId(page, rows, category.getCategoryId()));
        render(TemplateManager.me().getCurrentWebPath() + "/qa/category/index.html");
    }

    /**
     * 标签页
     * @param alias
     * @param page
     * @param request
     * @param model
     * @return
     */
    //@RequestMapping(value = "/tag/{alias}", method = RequestMethod.GET)
    public void tag() {
    	String alias = getPara(0);
		int page = getParaToInt(1, 1);
        // 系统id
		Example cmsSystemExample = CmsSystem.sql().andCodeEqualTo(CODE).example();
		CmsSystem system = CmsSystem.service().findFirst(cmsSystemExample);
        setAttr("system", system);
        // 当前标签
        Example cmsTagExample = CmsTag.sql().andSystemIdEqualTo(system.getSystemId())
                .andAliasEqualTo(alias).example();
        CmsTag tag = CmsTag.service().findFirst(cmsTagExample);
        setAttr("tag", tag);
        // 该标签文章列表
        int rows = 10;
        setAttr("articles", CmsArticle.service().paginateByTagId(page, rows, tag.getTagId()));
        render(TemplateManager.me().getCurrentWebPath() + "/qa/tag/index.html");
    }

    /**
     * 详情页
     * @param articleId
     * @param model
     * @return
     */
    //@RequestMapping(value = "/question/{articleId}", method = RequestMethod.GET)
    public void question() {
    	Long articleId = getParaToLong(0);
        CmsArticle article = CmsArticle.service().findById(articleId);
        setAttr("article", article);
        // 系统id
        Example cmsSystemExample = CmsSystem.sql().andCodeEqualTo(CODE).example();
		CmsSystem system = CmsSystem.service().findFirst(cmsSystemExample);
        setAttr("system", system);
        // 评论列表
        Example cmsCommentExample = CmsComment.sql()
                .andSystemIdEqualTo(system.getSystemId())
                .andArticleIdEqualTo(articleId)
                .andStatusEqualTo(1).example();
        cmsCommentExample.setOrderBy("ctime desc");
        List<CmsComment> comments = CmsComment.service().find(cmsCommentExample);
        setAttr("comments", comments);
        render(TemplateManager.me().getCurrentWebPath() + "/qa/question/index.html");
    }

    /**
     * 新增回复
     * @param articleId
     * @param cmsComment
     * @param request
     * @return
     */
    //@RequestMapping(value = "/answer/{articleId}", method = RequestMethod.POST)
    //@ResponseBody
    public void answer() {
    	Long articleId = getParaToLong("articleId");
    	CmsComment cmsComment = getModel(CmsComment.class, "comment");
    	// 系统id
    	Example cmsSystemExample = CmsSystem.sql().andCodeEqualTo(CODE).example();
		CmsSystem system = CmsSystem.service().findFirst(cmsSystemExample);
        long time = System.currentTimeMillis();
        cmsComment.setCtime(DateTimeUtils.parse(time));
        cmsComment.setArticleId(articleId);
        cmsComment.setUserId(USERID);
        cmsComment.setStatus(1);
        cmsComment.setIp(RequestUtils.getIpAddress(getRequest()));
        cmsComment.setAgent(getHeader("User-Agent"));
        cmsComment.setSystemId(system.getSystemId());
        boolean flag = cmsComment.save();
        renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
    }

}