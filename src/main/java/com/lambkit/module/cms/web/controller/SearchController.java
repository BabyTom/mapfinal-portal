package com.lambkit.module.cms.web.controller;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import com.jfinal.log.Log;

/**
 * 搜索控制器
 */
//@RequestMapping(value = "/search")
public class SearchController extends LambkitController {

	private static final Log LOG = Log.getLog(SearchController.class);

	//@RequestMapping(value = "/{keyword}", method = RequestMethod.GET)
	public void index() {
		String keyword = getPara(0);
		int page = getParaToInt(1, 1);
		String sort = getPara(2, "orders");
		String order = getPara(3, "desc");
		// 该关键字文章列表
		int rows = 10;
		Example cmsArticleExample = CmsArticle.sql().andStatusEqualTo(1).andTitleLike(keyword).example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsArticleExample.setOrderBy(sort + " " + order);
		}
		Page<CmsArticle> articles = CmsArticle.service().paginate(page, rows, cmsArticleExample);
		setAttr("articles", articles);
		render(TemplateManager.me().getCurrentWebPath() + "/search/index.html");
	}

}