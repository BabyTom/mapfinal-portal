package com.lambkit.module.cms.web.controller;

import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.CmsTopic;
import com.lambkit.web.controller.LambkitController;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.db.sql.column.Example;
import com.jfinal.log.Log;


/**
 * 专题首页控制器
 */
//@RequestMapping(value = "/topic")
public class TopicController extends LambkitController {

    private static final Log LOG = Log.getLog(TopicController.class);

    //@RequestMapping(value = "/list", method = RequestMethod.GET)
    public void list() {
    	int page = getParaToInt(0, 1);
        // 专题列表
        int rows = 10;
        Example cmsTopicExample = CmsTopic.sql().example();
        Page<CmsTopic> topics = CmsTopic.service().paginate(page, rows, cmsTopicExample);
        setAttr("topics", topics);
        render(TemplateManager.me().getCurrentWebPath() + "/topic/list.html");
    }

    //@RequestMapping(value = "{topicId}", method = RequestMethod.GET)
    public void index() {
    	Long topicId = getParaToLong(0);
        CmsTopic topic = CmsTopic.service().findById(topicId);
        setAttr("topic", topic);
        render(TemplateManager.me().getCurrentWebPath() + "/topic/index.html");
    }

}