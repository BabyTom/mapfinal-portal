package com.lambkit.module.cms.web.directive;

import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.module.cms.common.CmsConsts;
import com.lambkit.module.cms.common.CmsOptions;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.model.CmsArticleCategory;
import com.lambkit.web.LambkitControllerContext;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.LambkitPaginateDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

import javax.servlet.http.HttpServletRequest;

@JFinalDirective("articlePage")
public class ArticlePageDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Controller controller = LambkitControllerContext.get();

        int page = controller.getParaToInt(1, 1);
        int pageSize = getParaToInt("pageSize", scope, 10);
        String orderBy = getPara("orderBy", scope, "id desc");

        // 可以指定当前的分类ID
        Long categoryId = getParaToLong("categoryId", scope, 0L);
        CmsArticleCategory category = controller.getAttr("category");

        if (categoryId == 0 && category != null) {
            categoryId = category.getArticleCategoryId();
        }

        Page<CmsArticle> articlePage = categoryId == 0
                ? CmsArticle.service().paginateInNormal(page, pageSize, orderBy)
                : CmsArticle.service().paginateByCategoryIdInNormal(page, pageSize, categoryId, orderBy);

        scope.setGlobal("articlePage", articlePage);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }


    @JFinalDirective("articlePaginate")
    public static class TemplatePaginateDirective extends LambkitPaginateDirective {

        private boolean firstGotoIndex = false;

        @Override
        public void onRender(Env env, Scope scope, Writer writer) {
            firstGotoIndex = getPara("firstGotoIndex", scope, false);
            super.onRender(env, scope, writer);
        }

        @Override
        protected String getUrl(int pageNumber) {
            HttpServletRequest request = LambkitControllerContext.get().getRequest();
            String url = request.getRequestURI();
            String contextPath = JFinal.me().getContextPath();

            if (pageNumber == 1 && firstGotoIndex) {
                return contextPath + "/";
            }

            // 如果当前页面是首页的话
            // 需要改变url的值，因为 上一页或下一页是通过当前的url解析出来的
            if (url.equals(contextPath + "/")) {
                url = contextPath + "/article/category/index"
                        + CmsOptions.getAppUrlSuffix();
            }
            return DirectveKit.replacePageNumber(url, pageNumber);
        }

        @Override
        protected Page<?> getPage(Env env, Scope scope, Writer writer) {
            return (Page<?>) scope.get("articlePage");
        }

    }
}
