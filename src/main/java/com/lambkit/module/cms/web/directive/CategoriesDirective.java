package com.lambkit.module.cms.web.directive;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.module.cms.common.CmsActiveKit;
import com.lambkit.module.cms.core.layer.SortKit;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.model.CmsArticleCategory;
import com.lambkit.module.cms.rpc.model.CmsCategory;
import com.lambkit.web.LambkitControllerContext;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("categories")
public class CategoriesDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

    	Long systemId = getParaToLong("systemId", scope);
        String flag = getPara("flag", scope);
        String pflag = getPara("parentFlag", scope);
        Long pId = getParaToLong("parentId", scope);
        boolean asTree = getParaToBool("asTree", scope, Boolean.FALSE);

        List<CmsCategory> categories = CmsCategory.service().findListBySystemId(systemId);
        if (categories == null || categories.isEmpty()) {
            return;
        }

        SortKit.toLayer(categories);
        SortKit.fillParentAndChild(categories);


        if (StrKit.notBlank(pflag)) {
            categories = categories.stream().filter(category -> {
                CmsCategory parent = (CmsCategory) category.getParent();
                return parent != null && pflag.equals(parent.getFlag());
            }).collect(Collectors.toList());
        }

        if (pId != null) {
            categories = categories.stream().filter(category -> {
                CmsCategory parent = (CmsCategory) category.getParent();
                return parent != null && pId.equals(parent.getId());
            }).collect(Collectors.toList());
        }


        setActiveFlagByCurrentCategory(categories);
        setActiveFlagByCurrentArticle(categories);

        if (asTree == true) {
            SortKit.toTree(categories);
        }

        if (StrKit.notBlank(flag)) {
            categories = categories
                    .stream()
                    .filter(category -> flag.equals(category.getFlag()))
                    .collect(Collectors.toList());
        }

        if (categories == null || categories.isEmpty()) {
            return;
        }

        scope.setLocal("categories", categories);
        renderBody(env, scope, writer);
    }


    /**
     * 根据当前的分类，设置 高亮 标识
     *
     * @param categories
     */
    private void setActiveFlagByCurrentCategory(List<CmsCategory> categories) {

        CmsCategory currentCategory = LambkitControllerContext.get().getAttr("category");

        //当前页面并不是某个分类页面
        if (currentCategory == null) {
            return;
        }

        doFlagByCurrentCategory(categories, currentCategory);

    }


    /**
     * 根据当前的文章，设置 高亮 标识
     *
     * @param categories
     */
    private void setActiveFlagByCurrentArticle(List<CmsCategory> categories) {
        CmsArticle currentArticle = LambkitControllerContext.get().getAttr("article");

        //当前页面并不是文章详情页面
        if (currentArticle == null) {
            return;
        }

        List<CmsCategory> articleCategories = CmsCategory.service().findCategoryListByArticleId(currentArticle.getArticleId());
        if (articleCategories == null || articleCategories.isEmpty()) {
            return;
        }

        for (CmsCategory articleCategory : articleCategories) {
            doFlagByCurrentCategory(categories, articleCategory);
        }
    }


    private void doFlagByCurrentCategory(List<CmsCategory> categories, CmsCategory currentCategory) {
        for (CmsCategory category : categories) {
            if (currentCategory.getId().equals(category.getId())) {
                CmsActiveKit.makeItActive(category);
            }
        }
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
