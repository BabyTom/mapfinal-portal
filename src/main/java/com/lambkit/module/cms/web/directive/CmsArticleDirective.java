/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.web.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.common.util.StringUtils;
import com.lambkit.module.cms.rpc.api.CmsArticleService;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.web.directive.LambkitDirective;

public class CmsArticleDirective extends LambkitDirective {

	@Override
	public void onRender(Env env, Scope scope, Writer writer) {
		// TODO Auto-generated method stub
		String articleId = getPara("article_id", scope);
		String topicId = getPara("topic_id", scope);
		String title = getPara("title", scope);
		String author = getPara("author", scope);
		String fromurl = getPara("fromurl", scope);
		String image = getPara("image", scope);
		String keywords = getPara("keywords", scope);
		String description = getPara("description", scope);
		String type = getPara("type", scope);
		String allowcomments = getPara("allowcomments", scope);
		String status = getPara("status", scope);
		String content = getPara("content", scope);
		String userId = getPara("user_id", scope);
		String readnumber = getPara("readnumber", scope);
		String top = getPara("top", scope);
		String systemId = getPara("system_id", scope);
		String ctime = getPara("ctime", scope);
		String orders = getPara("orders", scope);
		int pagenum = getParaToInt("pagenum", scope, 0);
		int pagesize = getParaToInt("pagesize", scope, 0);
		String wheresql = getPara("sql", null);
		String sql = " from cms_article where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(articleId)) sql += " and article_id=" + articleId;//int unsigned
			if(StringUtils.hasText(topicId)) sql += " and topic_id=" + topicId;//int
			if(StringUtils.hasText(title)) sql += " and title like '%" + title + "%'";//varchar
			if(StringUtils.hasText(author)) sql += " and author like '%" + author + "%'";//varchar
			if(StringUtils.hasText(fromurl)) sql += " and fromurl like '%" + fromurl + "%'";//varchar
			if(StringUtils.hasText(image)) sql += " and image like '%" + image + "%'";//varchar
			if(StringUtils.hasText(keywords)) sql += " and keywords like '%" + keywords + "%'";//varchar
			if(StringUtils.hasText(description)) sql += " and description like '%" + description + "%'";//varchar
			if(StringUtils.hasText(type)) sql += " and type=" + type;//tinyint
			if(StringUtils.hasText(allowcomments)) sql += " and allowcomments=" + allowcomments;//tinyint
			if(StringUtils.hasText(status)) sql += " and status=" + status;//tinyint
			if(StringUtils.hasText(content)) sql += " and content like '%" + content + "%'";//varchar
			if(StringUtils.hasText(userId)) sql += " and user_id=" + userId;//int unsigned
			if(StringUtils.hasText(readnumber)) sql += " and readnumber=" + readnumber;//int unsigned
			if(StringUtils.hasText(top)) sql += " and top=" + top;//int
			if(StringUtils.hasText(systemId)) sql += " and system_id=" + systemId;//int unsigned
			if(StringUtils.hasText(ctime)) sql += " and ctime=" + ctime;//bigint unsigned
			if(StringUtils.hasText(orders)) sql += " and orders=" + orders;//bigint unsigned
		} else {
			sql += wheresql;
		}
		
		String orderby = getPara("orderby", scope, null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		CmsArticleService service = CmsArticle.service();
		
		String tagEntityKeyname = getPara("key", scope, "entity");
		if(pagenum==0) {
			scope.set(tagEntityKeyname, service.dao().findFirst("select *" + sql));
		} else {
			if(pagesize==0) {
				scope.set(tagEntityKeyname, service.dao().find("select *" + sql));
			} else {
				scope.set(tagEntityKeyname, service.dao().paginate(pagenum, pagesize, "select *", sql));
			}
		}
        renderBody(env, scope, writer);
	}

}
