/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.web.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.common.util.StringUtils;
import com.lambkit.module.cms.rpc.api.CmsCommentService;
import com.lambkit.module.cms.rpc.model.CmsComment;
import com.lambkit.web.directive.LambkitDirective;

public class CmsCommentDirective extends LambkitDirective {

	@Override
	public void onRender(Env env, Scope scope, Writer writer) {
		// TODO Auto-generated method stub
		String commentId = getPara("comment_id", scope);
		String pid = getPara("pid", scope);
		String articleId = getPara("article_id", scope);
		String userId = getPara("user_id", scope);
		String content = getPara("content", scope);
		String status = getPara("status", scope);
		String ip = getPara("ip", scope);
		String agent = getPara("agent", scope);
		String systemId = getPara("system_id", scope);
		String ctime = getPara("ctime", scope);
		int pagenum = getParaToInt("pagenum", scope, 0);
		int pagesize = getParaToInt("pagesize", scope, 0);
		String wheresql = getPara("sql", null);
		String sql = " from cms_comment where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(commentId)) sql += " and comment_id=" + commentId;//int unsigned
			if(StringUtils.hasText(pid)) sql += " and pid=" + pid;//int unsigned
			if(StringUtils.hasText(articleId)) sql += " and article_id=" + articleId;//int unsigned
			if(StringUtils.hasText(userId)) sql += " and user_id=" + userId;//int unsigned
			if(StringUtils.hasText(content)) sql += " and content like '%" + content + "%'";//varchar
			if(StringUtils.hasText(status)) sql += " and status=" + status;//tinyint
			if(StringUtils.hasText(ip)) sql += " and ip like '%" + ip + "%'";//varchar
			if(StringUtils.hasText(agent)) sql += " and agent like '%" + agent + "%'";//varchar
			if(StringUtils.hasText(systemId)) sql += " and system_id=" + systemId;//int unsigned
			if(StringUtils.hasText(ctime)) sql += " and ctime=" + ctime;//bigint
		} else {
			sql += wheresql;
		}
		
		String orderby = getPara("orderby", scope, null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		CmsCommentService service = CmsComment.service();
		
		String tagEntityKeyname = getPara("key", scope, "entity");
		if(pagenum==0) {
			scope.set(tagEntityKeyname, service.dao().findFirst("select *" + sql));
		} else {
			if(pagesize==0) {
				scope.set(tagEntityKeyname, service.dao().find("select *" + sql));
			} else {
				scope.set(tagEntityKeyname, service.dao().paginate(pagenum, pagesize, "select *", sql));
			}
		}
        renderBody(env, scope, writer);
	}

}
