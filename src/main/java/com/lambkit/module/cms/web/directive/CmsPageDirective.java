/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.web.directive;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.common.util.StringUtils;
import com.lambkit.module.cms.rpc.api.CmsPageService;
import com.lambkit.module.cms.rpc.model.CmsPage;
import com.lambkit.web.directive.LambkitDirective;

public class CmsPageDirective extends LambkitDirective {

	@Override
	public void onRender(Env env, Scope scope, Writer writer) {
		// TODO Auto-generated method stub
		String pageId = getPara("page_id", scope);
		String pid = getPara("pid", scope);
		String title = getPara("title", scope);
		String alias = getPara("alias", scope);
		String content = getPara("content", scope);
		String keywords = getPara("keywords", scope);
		String description = getPara("description", scope);
		String ctime = getPara("ctime", scope);
		String orders = getPara("orders", scope);
		int pagenum = getParaToInt("pagenum", scope, 0);
		int pagesize = getParaToInt("pagesize", scope, 0);
		String wheresql = getPara("sql", null);
		String sql = " from cms_page where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(pageId)) sql += " and page_id=" + pageId;//int unsigned
			if(StringUtils.hasText(pid)) sql += " and pid=" + pid;//int
			if(StringUtils.hasText(title)) sql += " and title like '%" + title + "%'";//varchar
			if(StringUtils.hasText(alias)) sql += " and alias like '%" + alias + "%'";//varchar
			if(StringUtils.hasText(content)) sql += " and content like '%" + content + "%'";//varchar
			if(StringUtils.hasText(keywords)) sql += " and keywords like '%" + keywords + "%'";//varchar
			if(StringUtils.hasText(description)) sql += " and description like '%" + description + "%'";//varchar
			if(StringUtils.hasText(ctime)) sql += " and ctime=" + ctime;//bigint
			if(StringUtils.hasText(orders)) sql += " and orders=" + orders;//bigint
		} else {
			sql += wheresql;
		}
		
		String orderby = getPara("orderby", scope, null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		CmsPageService service = CmsPage.service();
		
		String tagEntityKeyname = getPara("key", scope, "entity");
		if(pagenum==0) {
			scope.set(tagEntityKeyname, service.dao().findFirst("select *" + sql));
		} else {
			if(pagesize==0) {
				scope.set(tagEntityKeyname, service.dao().find("select *" + sql));
			} else {
				scope.set(tagEntityKeyname, service.dao().paginate(pagenum, pagesize, "select *", sql));
			}
		}
        renderBody(env, scope, writer);
	}

}
