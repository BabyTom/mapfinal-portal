package com.lambkit.module.cms.web.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.web.LambkitControllerContext;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("nextArticle")
public class NextArticleDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        CmsArticle article = LambkitControllerContext.get().getAttr("article");

        CmsArticle nextArticle = CmsArticle.service().findNextById(article.getArticleId());
        if (nextArticle == null) {
            return;
        }

        scope.setLocal("next", nextArticle);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
