package com.lambkit.module.cms.web.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.web.LambkitControllerContext;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

/**
 * 
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("previousArticle")
public class PreviousArticleDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        CmsArticle article = LambkitControllerContext.get().getAttr("article");

        CmsArticle previousArticle = CmsArticle.service().findPreviousById(article.getArticleId());
        if (previousArticle == null) {
            return;
        }

        scope.setLocal("previous", previousArticle);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
