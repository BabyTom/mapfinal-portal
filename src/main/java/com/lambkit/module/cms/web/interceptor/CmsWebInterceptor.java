package com.lambkit.module.cms.web.interceptor;

import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.CmsMenu;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Log;


import java.util.List;

/**
 * 公共拦截器
 */
public class CmsWebInterceptor implements Interceptor {

    private static final Log LOG = Log.getLog(CmsWebInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		// 过滤ajax
        if (null != controller.getHeader("X-Requested-With") && "XMLHttpRequest".equalsIgnoreCase(controller.getHeader("X-Requested-With"))) {
        	System.out.println("X-Requested-With: " + TemplateManager.me().getCurrentWebPath());
        	inv.invoke();
        } else {
        	// zheng-ui静态资源配置信息
            //String appName = PropKit.get("app.name");
            //String uiPath = PropKit.get("zheng.ui.path");
            //controller.set("appName", appName);
            //controller.set("uiPath", uiPath);
        	controller.set("TPATH", TemplateManager.me().getCurrentWebPath());
            // 菜单
            List<CmsMenu> menus = CmsMenu.service().find(CmsMenu.sql().example().setOrderBy("orders asc"));
            controller.set("menus", menus);
    		inv.invoke();
        }
	}
}
