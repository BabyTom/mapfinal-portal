/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.web.tag;

import java.io.IOException;
import java.util.Map;

import com.lambkit.module.cms.rpc.model.CmsTag;
import com.lambkit.web.tag.LambkitTemplateModel;
import com.lambkit.module.cms.rpc.api.CmsTagService;
import com.lambkit.common.util.StringUtils;
import com.lambkit.common.service.ServiceKit;
import com.jfinal.kit.StrKit;
import com.jfinal.render.FreeMarkerRender;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-28
 * @version 1.0
 * @since 1.0
 */
/**
 * cms_tag标签<br>
 * 参数：{id:主键}
 * 返回值：{entity:cms_tag信息}
 * @author lambkit
 */
public class CmsTagMarker extends LambkitTemplateModel {

	@Override
	public void onRender(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		// TODO Auto-generated method stub
		String tagId = get(params, "tag_id");
		String name = get(params, "name");
		String description = get(params, "description");
		String icon = get(params, "icon");
		String type = get(params, "type");
		String alias = get(params, "alias");
		String systemId = get(params, "system_id");
		String ctime = get(params, "ctime");
		String orders = get(params, "orders");
		int pagenum = getInt(params, "pagenum", 0);
		int pagesize = getInt(params, "pagesize", 0);
		String wheresql = get(params, "sql", null);
		String sql = " from cms_tag where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(tagId)) sql += " and tag_id=" + tagId;//int unsigned
			if(StringUtils.hasText(name)) sql += " and name like '%" + name + "%'";//varchar
			if(StringUtils.hasText(description)) sql += " and description like '%" + description + "%'";//varchar
			if(StringUtils.hasText(icon)) sql += " and icon like '%" + icon + "%'";//varchar
			if(StringUtils.hasText(type)) sql += " and type=" + type;//tinyint
			if(StringUtils.hasText(alias)) sql += " and alias like '%" + alias + "%'";//varchar
			if(StringUtils.hasText(systemId)) sql += " and system_id=" + systemId;//int unsigned
			if(StringUtils.hasText(ctime)) sql += " and ctime=" + ctime;//bigint unsigned
			if(StringUtils.hasText(orders)) sql += " and orders=" + orders;//bigint unsigned
		} else {
			sql += wheresql;
		}
		
		String orderby = get(params, "orderby", null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		CmsTagService service = CmsTag.service();
		
		String tagEntityKeyname = get(params, "key", "entity");
		if(pagenum==0) {
			env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
					service.dao().findFirst("select *" + sql)));
		} else {
			if(pagesize==0) {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().find("select *" + sql)));
			} else {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().paginate(pagenum, pagesize, "select *", sql)));
			}
		}
        body.render(env.getOut());
	}
}
