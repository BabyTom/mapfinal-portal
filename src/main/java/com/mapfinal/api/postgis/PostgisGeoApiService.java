package com.mapfinal.api.postgis;

import java.util.List;

import org.locationtech.jts.geom.Geometry;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.mapfinal.api.service.GeoApiService;

public class PostgisGeoApiService implements GeoApiService {

	@Override
	public List<Record> distance(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Distance(geography(geom), st_geogfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}
	
	@Override
	public List<Record> dwithin(String layerName, Geometry geometry, float radius) {
		// TODO Auto-generated method stub
		//SELECT *, st_astext(geom) FROM "public"."map_town_point" WHERE st_dwithin(geography(geom), st_geogfromtext('POINT(117.8153708 31.97944503)'), 2000) ;
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE st_dwithin(geography(geom), st_geogfromtext('");
		sql.append(geometry.toText());
		sql.append("'), ");
		sql.append(radius);
		sql.append(")");
		return Db.find(sql.toString());
	}


	@Override
	public List<Record> equals(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Equals(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> disjoin(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Disjoint(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> intersects(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Intersects(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> touches(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Touches(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> crosses(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Crosses(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> within(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Within(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> overlaps(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Overlaps(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}
	
	@Override
	public List<Record> contains(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		//SELECT *, st_astext(geom) FROM "public"."map_city" WHERE ST_Contains(geom, st_geomfromtext('POINT(117.8153708 31.97944503)')) ;
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Contains(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> covers(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_Covers(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

	@Override
	public List<Record> coveredby(String layerName, Geometry geometry) {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append("select * from \"");
		sql.append(layerName);
		sql.append("\" WHERE ST_CoveredBy(geom, st_geomfromtext('");
		sql.append(geometry.toText());
		sql.append("'))");
		return Db.find(sql.toString());
	}

}
