package com.mapfinal.api.service;

import java.util.List;

import org.locationtech.jts.geom.Geometry;

import com.jfinal.plugin.activerecord.Record;

public interface GeoApiService {

	/**
	 * ST_Distance(geometry, geometry)
	返回两个几何对象的距离（笛卡儿距离），不使用索引。
	 */
	//@ApiMapping(value = "distance")
	public List<Record> distance(String layerName, Geometry geometry);
	
	/**
	 * 点缓冲区查询特定图层
	 * 查a点指定半径内的所有point类型记录
	 * ST_DWithid(geometry, geometry, float)
	 * 如果一个几何对象(geometry)在另一个几何对象描述的距离(float)内，返回TRUE。如果有索引，会用到索引。
	 */
	//@ApiMapping(value = "dwithin")
	public List<Record> dwithin(String layerName, Geometry geometry, float dist);
	
	/**
	 * ST_Equals(geometry, geometry)
	如果两个空间对象相等，则返回TRUE。用这个函数比用“=”更好，例如：
	equals(‘LINESTRING(0 0, 10 10)’,‘LINESTRING(0 0, 5 5, 10 10)’) 返回 TRUE。
	 */
	//@ApiMapping(value = "equals")
	public List<Record> equals(String layerName, Geometry geometry);
	
	/**
	 * ST_Disjoint(geometry, geometry)
	如果两个对象不相连，则返回TRUE。不要使用GeometryCollection作为参数。
	 */
	//@ApiMapping(value = "disjoin")
	public List<Record> disjoin(String layerName, Geometry geometry);
	
	/**
	 * ST_Intersects(geometry, geometry)
	判断两个几何空间数据是否相交,如果相交返回true,不要使用GeometryCollection作为参数。
	Intersects(g1, g2 ) --> Not (Disjoint(g1, g2 ))
	不使用索引可以用_ST_Intersects.
	 */
	//@ApiMapping(value = "intersects")
	public List<Record> intersects(String layerName, Geometry geometry);
	
	/**
	 * ST_Touches(geometry, geometry)
	如果两个几何空间对象存在接触，则返回TRUE。不要使用GeometryCollection作为参数。
	a.Touches(b) -> (I(a) intersection I(b) = {empty set} ) and (a intersectionb) not empty
	不使用索引可以用_ST_Touches.
	 */
	//@ApiMapping(value = "touches")
	public List<Record> touches(String layerName, Geometry geometry);
	
	/**
	 * ST_Crosses(geometry, geometry)
	如果两个几何空间对象存在交叉，则返回TRUE。不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Crosses.
	 */
	//@ApiMapping(value = "crosses")
	public List<Record> crosses(String layerName, Geometry geometry);
	
	/**
	 * ST_Within(geometry A, geometry B)
	如果几何空间对象A存在空间对象B中,则返回TRUE,不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Within
	 */
	//@ApiMapping(value = "within")
	public List<Record> within(String layerName, Geometry geometry);
	
	/**
	 * ST_Overlaps(geometry, geometry)
	如果两个几何空间数据存在交迭,则返回 TRUE,不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Overlaps.
	 */
	//@ApiMapping(value = "overlaps")
	public List<Record> overlaps(String layerName, Geometry geometry);
	
	/**
	 * ST_Contains(geometry A, geometry B)
	如果几何空间对象A包含空间对象B,则返回 TRUE,不要使用GeometryCollection作为参数。
	这个函数类似于ST_Within(geometry B, geometryA)
	不使用索引可以用_ST_Contains.
	 * 点查询是否属于面图斑内
	 */
	//@ApiMapping(value = "contains")
	public List<Record> contains(String layerName, Geometry geometry);
	
	/**
	 * ST_Covers(geometry A, geometry B)
	如果几何空间对象B中的所有点都在空间对象A中,则返回 TRUE。
	不要使用GeometryCollection作为参数。
	不使用索引可以用_ST_Covers.
	 */
	//@ApiMapping(value = "covers")
	public List<Record> covers(String layerName, Geometry geometry);
	
	/**
	 * ST_CoveredBy(geometry A, geometry B)
	如果几何空间对象A中的所有点都在空间对象B中,则返回 TRUE。
	 */
	//@ApiMapping(value = "coveredby")
	public List<Record> coveredby(String layerName, Geometry geometry);
	
}
