package com.mapfinal.api.service;

import com.jfinal.aop.Aop;
import com.mapfinal.api.postgis.PostgisGeoApiService;
import com.mapfinal.data.StoreType;

public class GeoApiServiceFactory {

	public GeoApiService create(StoreType type) {
		if(type == StoreType.POSTGIS)  {
			return Aop.get(PostgisGeoApiService.class);
		}
		return null;
	}
}
