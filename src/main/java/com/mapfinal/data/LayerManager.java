package com.mapfinal.data;

import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.lambkit.common.util.EncryptUtils;
import com.lambkit.core.aop.AopKit;

public class LayerManager {

	private static LayerManager manager = null;
	
	public static LayerManager me() {
		if(manager==null) {
			manager = AopKit.singleton(LayerManager.class);
		}
		return manager;
	}
	
	//private String workspace;
	/**
	 * 
	 */
	private Map<String, MapLayer> layers;
	
	/**
	 * 加入空间图层
	 * @param layer
	 */
	public void addLayer(MapLayer layer) {
		if(layers==null) {
			layers = Maps.newHashMap();
		}
		String name = layer.getStoreName() + ":" + layer.getName();
		layers.put(EncryptUtils.md5(name), layer);
	}
	
	public MapLayer getLayer(String name) {
		if(layers!=null)  {
			return layers.get(name);
		}
		return null;
	}
	/*
	public String getWorkspace() {
		return workspace;
	}
	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}
	*/
	public Map<String, MapLayer> getLayers() {
		return layers;
	}

	public void setLayers(Map<String, MapLayer> layers) {
		this.layers = layers;
	}
}
