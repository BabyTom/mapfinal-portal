package com.mapfinal.data;

import java.io.File;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Kv;
import com.lambkit.common.util.FileUtils;
import com.lambkit.common.util.PathUtils;
import com.lambkit.db.datasource.DataSourceConfig;
import com.mapfinal.data.postgis.PostgisStore;
import com.mapfinal.data.shpfile.ShapefileStore;

public class MapStoreFactory implements StoreFactory {

	@Override
	public void dump(MapStore store) {
		// TODO Auto-generated method stub
		if(store instanceof PostgisStore) {
			PostgisStore pstore = (PostgisStore) store;
			Kv data = Kv.by("type", "postgres");
			data.set("name", pstore.getName());
			data.set("config", pstore.getConfig());
			String json = JsonKit.toJson(data);
			save(store.getName(), json);
		} else if(store instanceof ShapefileStore) {
			ShapefileStore pstore = (ShapefileStore) store;
			Kv data = Kv.by("type", "shapefile");
			data.set("name", pstore.getName());
			data.set("path", pstore.getPath());
			String json = JsonKit.toJson(data);
			save(store.getName(), json);
		}
	}

	@Override
	public MapStore load(String path) {
		// TODO Auto-generated method stub
		File file = new File(path + File.separator + "store.js");
		String json = FileUtils.readString(file);
		JSONObject store = JSON.parseObject(json);
		String type = store.getString("type");
		if("postgres".equalsIgnoreCase(type)) {
			DataSourceConfig config = JsonKit.parse(store.getString("config"), DataSourceConfig.class);
			return PostgisStore.create(config, true);
		} else if("shapefile".equalsIgnoreCase(type)) {
			String storeName = store.getString("name");
			String storePath = store.getString("path");
			return ShapefileStore.create(storeName, storePath);
		}
		return null;
	}

	
	private void save(String storeName, String json) {
		String path = StoreManager.me().getWorkspacePath() + File.separator + storeName;
		PathUtils.createDirectory(path);
		File file = new File(path + File.separator + "store.js");
		FileUtils.writeString(file, json);
	}
}
