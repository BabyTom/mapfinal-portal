package com.mapfinal.data;

public interface StoreFactory {

	/**
	 * 持久化
	 */
	void dump(MapStore store);
	/**
	 * 加载配置
	 */
	MapStore load(String path);
	
}
