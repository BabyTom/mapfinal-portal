package com.mapfinal.data;

public enum StoreType {

	POSTGIS("postgis"), 
	SHPFILE("shpfile"), 
	GEOTIFF("geotiff");

    private final String value;

    StoreType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
