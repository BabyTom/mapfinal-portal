package com.mapfinal.data.controller;

import com.jfinal.kit.StrKit;
import com.lambkit.core.api.route.ApiResult;
import com.lambkit.web.controller.LambkitController;
import com.mapfinal.data.LayerManager;
import com.mapfinal.data.MapStore;
import com.mapfinal.data.StoreManager;

public class MapDataController extends LambkitController {

	public void index() {
		renderText("Welcome to visit MapData website!");
	}
	
	public void store() {
		String type = getPara(0);
		ApiResult ret = null;
		if("data".equalsIgnoreCase(type)) {
			String storeName = getPara(1);
			MapStore store = null;
			if(StrKit.isBlank(storeName)) {
				store = StoreManager.me().getDefaultStore();
			} else {
				store = StoreManager.me().getStore(storeName);
			}
			if(store!=null) {
				ret = ApiResult.ok("success", store.getDatas());
			} else {
				ret = ApiResult.fail("store is null", null);
			}
		} else if("layer".equalsIgnoreCase(type)) {
			String storeName = getPara(1);
			MapStore store = null;
			if(StrKit.isBlank(storeName)) {
				store = StoreManager.me().getDefaultStore();
			} else {
				store = StoreManager.me().getStore(storeName);
			}
			if(store!=null) {
				String layerName = getPara(2);
				if(StrKit.isBlank(layerName)) {
					ret = ApiResult.ok("success", store.getLayers());
				} else {
					ret = ApiResult.ok("success", store.getLayer(layerName));
				}
			} else {
				ret = ApiResult.fail("store is null", null);
			}
		} else if("info".equalsIgnoreCase(type)) { 
			String storeName = getPara(1);
			MapStore store = null;
			if(StrKit.isBlank(storeName)) {
				store = StoreManager.me().getDefaultStore();
			} else {
				store = StoreManager.me().getStore(storeName);
			}
			ret = ApiResult.ok("success", store.info());
		} else {
			ret = ApiResult.ok("success", StoreManager.me().getStores().keySet());
		}
		renderJson(ret);
	}
	
	public void layer() {
		String layerName = getPara(0);
		ApiResult ret = null;
		if(StrKit.isBlank(layerName)) {
			ret = ApiResult.ok("success", LayerManager.me().getLayers());
		} else {
			ret = ApiResult.ok("success", LayerManager.me().getLayer(layerName));
		}
		renderJson(ret);
	}
}
