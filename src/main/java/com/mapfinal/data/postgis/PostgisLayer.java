package com.mapfinal.data.postgis;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.data.LayerManager;
import com.mapfinal.data.StoreManager;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

public class PostgisLayer implements MapLayer {

	/**
	 * 图层名称
	 */
	private String name;
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 本名，文件名或表格名称
	 */
	private String nativeName;
	private String storeName;
	private String defaultStyle;
	private String SRS;
	private boolean publish;

	private List<ServerLayer> serverLayers;
	private List<String> dataApis;

	public static PostgisLayer create(Map<String, String> options) {
		if (options == null)
			return null;
		PostgisLayer layer = new PostgisLayer();
		layer.setName(options.get("name"));
		layer.setTitle(options.get("title"));
		layer.setNativeName(options.get("nativeName"));
		layer.setStoreName(options.get("storename"));
		layer.setDefaultStyle(options.get("defaultStyle"));
		layer.setSRS(options.get("rsr"));
		if (StrKit.notBlank(layer.getName()) && StrKit.notBlank(layer.getNativeName())
				&& StrKit.notBlank(layer.getStoreName())) {
			StoreManager.me().getStore(layer.getStoreName()).publish(layer.getNativeName(), layer);
			return layer;
		} else {
			return null;
		}
	}
	
	@Override
	public boolean publish(Server server) {
		if(server==null) return false;
		setPublish(true);
		//addServerLayer(serverLayer);
		LayerManager.me().addLayer(this);
		return true;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	public String getNativeName() {
		// TODO Auto-generated method stub
		return nativeName;
	}

	@Override
	public String getDefaultStyle() {
		// TODO Auto-generated method stub
		return defaultStyle;
	}

	@Override
	public String getStoreName() {
		// TODO Auto-generated method stub
		return storeName;
	}

	@Override
	public String getSRS() {
		// TODO Auto-generated method stub
		return SRS;
	}

	@Override
	public boolean isPublish() {
		// TODO Auto-generated method stub
		return publish;
	}

	@Override
	public boolean publishLayer() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<String> getDataApis() {
		// TODO Auto-generated method stub
		return dataApis;
	}

	public void addDataApi(String api) {
		if (StrKit.isBlank(api))
			return;
		if (dataApis == null) {
			dataApis = Lists.newArrayList();
		}
		dataApis.add(api);
	}

	@Override
	public List<ServerLayer> getServerLayers() {
		// TODO Auto-generated method stub
		return serverLayers;
	}

	public void addServerLayer(ServerLayer serverLayer) {
		if (serverLayer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Lists.newArrayList();
		}
		serverLayers.add(serverLayer);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public void setDefaultStyle(String defaultStyle) {
		this.defaultStyle = defaultStyle;
	}

	public void setSRS(String sRS) {
		SRS = sRS;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	public void setServerLayers(List<ServerLayer> serverLayers) {
		this.serverLayers = serverLayers;
	}

}
