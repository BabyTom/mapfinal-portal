/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis;

import com.jfinal.config.Routes;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceManager;
import com.lambkit.core.rpc.RpcConfig;
import com.lambkit.db.datasource.ActiveRecordPluginWrapper;
import com.lambkit.module.LambkitModule;
import com.lambkit.module.annotation.Module;
import com.mapfinal.gis.GisConfig;
import com.mapfinal.gis.model.GisApi;
import com.mapfinal.gis.model.GisLayer;
import com.mapfinal.gis.model.GisLayerApi;
import com.mapfinal.gis.model.GisLayerFileShape;
import com.mapfinal.gis.model.GisLayerGroup;
import com.mapfinal.gis.model.GisLayerTablePostgis;
import com.mapfinal.gis.model.GisMap;
import com.mapfinal.gis.model.GisMapLayer;
import com.mapfinal.gis.model.GisMapServer;
import com.mapfinal.gis.model.GisMapStyle;
import com.mapfinal.gis.model.GisServer;
import com.mapfinal.gis.model.GisServerApi;
import com.mapfinal.gis.model.GisServerLayer;
import com.mapfinal.gis.model.GisStyle;
import com.mapfinal.gis.service.GisApiService;
import com.mapfinal.gis.service.GisLayerApiService;
import com.mapfinal.gis.service.GisLayerFileShapeService;
import com.mapfinal.gis.service.GisLayerGroupService;
import com.mapfinal.gis.service.GisLayerService;
import com.mapfinal.gis.service.GisLayerTablePostgisService;
import com.mapfinal.gis.service.GisMapLayerService;
import com.mapfinal.gis.service.GisMapServerService;
import com.mapfinal.gis.service.GisMapService;
import com.mapfinal.gis.service.GisMapStyleService;
import com.mapfinal.gis.service.GisServerApiService;
import com.mapfinal.gis.service.GisServerLayerService;
import com.mapfinal.gis.service.GisServerService;
import com.mapfinal.gis.service.GisStyleService;
import com.mapfinal.gis.service.impl.GisApiServiceImpl;
import com.mapfinal.gis.service.impl.GisApiServiceMock;
import com.mapfinal.gis.service.impl.GisLayerApiServiceImpl;
import com.mapfinal.gis.service.impl.GisLayerApiServiceMock;
import com.mapfinal.gis.service.impl.GisLayerFileShapeServiceImpl;
import com.mapfinal.gis.service.impl.GisLayerFileShapeServiceMock;
import com.mapfinal.gis.service.impl.GisLayerGroupServiceImpl;
import com.mapfinal.gis.service.impl.GisLayerGroupServiceMock;
import com.mapfinal.gis.service.impl.GisLayerServiceImpl;
import com.mapfinal.gis.service.impl.GisLayerServiceMock;
import com.mapfinal.gis.service.impl.GisLayerTablePostgisServiceImpl;
import com.mapfinal.gis.service.impl.GisLayerTablePostgisServiceMock;
import com.mapfinal.gis.service.impl.GisMapLayerServiceImpl;
import com.mapfinal.gis.service.impl.GisMapLayerServiceMock;
import com.mapfinal.gis.service.impl.GisMapServerServiceImpl;
import com.mapfinal.gis.service.impl.GisMapServerServiceMock;
import com.mapfinal.gis.service.impl.GisMapServiceImpl;
import com.mapfinal.gis.service.impl.GisMapServiceMock;
import com.mapfinal.gis.service.impl.GisMapStyleServiceImpl;
import com.mapfinal.gis.service.impl.GisMapStyleServiceMock;
import com.mapfinal.gis.service.impl.GisServerApiServiceImpl;
import com.mapfinal.gis.service.impl.GisServerApiServiceMock;
import com.mapfinal.gis.service.impl.GisServerLayerServiceImpl;
import com.mapfinal.gis.service.impl.GisServerLayerServiceMock;
import com.mapfinal.gis.service.impl.GisServerServiceImpl;
import com.mapfinal.gis.service.impl.GisServerServiceMock;
import com.mapfinal.gis.service.impl.GisStyleServiceImpl;
import com.mapfinal.gis.service.impl.GisStyleServiceMock;
import com.mapfinal.gis.web.directive.GisApiDirective;
import com.mapfinal.gis.web.directive.GisLayerApiDirective;
import com.mapfinal.gis.web.directive.GisLayerDirective;
import com.mapfinal.gis.web.directive.GisLayerFileShapeDirective;
import com.mapfinal.gis.web.directive.GisLayerGroupDirective;
import com.mapfinal.gis.web.directive.GisLayerTablePostgisDirective;
import com.mapfinal.gis.web.directive.GisMapDirective;
import com.mapfinal.gis.web.directive.GisMapLayerDirective;
import com.mapfinal.gis.web.directive.GisMapServerDirective;
import com.mapfinal.gis.web.directive.GisMapStyleDirective;
import com.mapfinal.gis.web.directive.GisServerApiDirective;
import com.mapfinal.gis.web.directive.GisServerDirective;
import com.mapfinal.gis.web.directive.GisServerLayerDirective;
import com.mapfinal.gis.web.directive.GisStyleDirective;
import com.mapfinal.gis.web.tag.GisApiTag;
import com.mapfinal.gis.web.tag.GisLayerApiTag;
import com.mapfinal.gis.web.tag.GisLayerFileShapeTag;
import com.mapfinal.gis.web.tag.GisLayerGroupTag;
import com.mapfinal.gis.web.tag.GisLayerTablePostgisTag;
import com.mapfinal.gis.web.tag.GisLayerTag;
import com.mapfinal.gis.web.tag.GisMapLayerTag;
import com.mapfinal.gis.web.tag.GisMapServerTag;
import com.mapfinal.gis.web.tag.GisMapStyleTag;
import com.mapfinal.gis.web.tag.GisMapTag;
import com.mapfinal.gis.web.tag.GisServerApiTag;
import com.mapfinal.gis.web.tag.GisServerLayerTag;
import com.mapfinal.gis.web.tag.GisServerTag;
import com.mapfinal.gis.web.tag.GisStyleTag;

/**
 * 地理信息系统，管理各种地理信息。
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
@Module
public class GisModule extends LambkitModule  {

	@Override
	public void configMapping(ActiveRecordPluginWrapper arp) {
		if(StrKit.isBlank(getConfig().getDbconfig())) {
			mapping(arp);
		}
	}
	
	@Override
	public void configMapping(String name, ActiveRecordPluginWrapper arp) {
		super.configMapping(name, arp);
		if(StrKit.notBlank(name) && name.equals(getConfig().getDbconfig())) {
			mapping(arp);
		}
	}
	
	@Override
	public void configEngine(Engine engine) {
		// TODO Auto-generated method stub
		super.configEngine(engine);
		addDirective(engine, "gisApi", GisApiDirective.class);
		addDirective(engine, "gisLayer", GisLayerDirective.class);
		addDirective(engine, "gisLayerApi", GisLayerApiDirective.class);
		addDirective(engine, "gisLayerFileShape", GisLayerFileShapeDirective.class);
		addDirective(engine, "gisLayerGroup", GisLayerGroupDirective.class);
		addDirective(engine, "gisLayerTablePostgis", GisLayerTablePostgisDirective.class);
		addDirective(engine, "gisMap", GisMapDirective.class);
		addDirective(engine, "gisMapLayer", GisMapLayerDirective.class);
		addDirective(engine, "gisMapServer", GisMapServerDirective.class);
		addDirective(engine, "gisMapStyle", GisMapStyleDirective.class);
		addDirective(engine, "gisServer", GisServerDirective.class);
		addDirective(engine, "gisServerApi", GisServerApiDirective.class);
		addDirective(engine, "gisServerLayer", GisServerLayerDirective.class);
		addDirective(engine, "gisStyle", GisStyleDirective.class);
	}
	
	@Override
	public void configRoute(Routes me) {
		// TODO Auto-generated method stub
		me.add(new GisRoute());
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		addTag(this);
		if("server".equals(getConfig().getServerType())) {
			registerLocalService();
		} else if("client".equals(getConfig().getServerType())) {
			registerRemoteService();
		} 
	}
	
	public void mapping(ActiveRecordPluginWrapper arp) {
		arp.addMapping("gis_api", "id", GisApi.class);
		arp.addMapping("gis_layer", "layerkey", GisLayer.class);
		arp.addMapping("gis_layer_api", "layerkey,api_id", GisLayerApi.class);
		arp.addMapping("gis_layer_file_shape", "layerkey,fileid", GisLayerFileShape.class);
		arp.addMapping("gis_layer_group", "layer_group_key,layer_key", GisLayerGroup.class);
		arp.addMapping("gis_layer_table_postgis", "layerkey,tbid", GisLayerTablePostgis.class);
		arp.addMapping("gis_map", "id", GisMap.class);
		arp.addMapping("gis_map_layer", "map_id,layer_key", GisMapLayer.class);
		arp.addMapping("gis_map_server", "map_id,server_key", GisMapServer.class);
		arp.addMapping("gis_map_style", "map_id,style_id", GisMapStyle.class);
		arp.addMapping("gis_server", "serverkey", GisServer.class);
		arp.addMapping("gis_server_api", "id", GisServerApi.class);
		arp.addMapping("gis_server_layer", "server_key,layer_key", GisServerLayer.class);
		arp.addMapping("gis_style", "id", GisStyle.class);
	}
	
	public void addTag(LambkitModule lk) {
		lk.addTag("gisApi", new GisApiTag());
		lk.addTag("gisLayer", new GisLayerTag());
		lk.addTag("gisLayerApi", new GisLayerApiTag());
		lk.addTag("gisLayerFileShape", new GisLayerFileShapeTag());
		lk.addTag("gisLayerGroup", new GisLayerGroupTag());
		lk.addTag("gisLayerTablePostgis", new GisLayerTablePostgisTag());
		lk.addTag("gisMap", new GisMapTag());
		lk.addTag("gisMapLayer", new GisMapLayerTag());
		lk.addTag("gisMapServer", new GisMapServerTag());
		lk.addTag("gisMapStyle", new GisMapStyleTag());
		lk.addTag("gisServer", new GisServerTag());
		lk.addTag("gisServerApi", new GisServerApiTag());
		lk.addTag("gisServerLayer", new GisServerLayerTag());
		lk.addTag("gisStyle", new GisStyleTag());
	}
			
	public void registerLocalService() {
		registerLocalService(getRpcGroup(), getRpcVersion(), getRpcPort());
	}
	
	public void registerLocalService(String group, String version, int port) {
		ServiceManager.me().mapping(GisApiService.class, GisApiServiceImpl.class, GisApiServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisLayerService.class, GisLayerServiceImpl.class, GisLayerServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisLayerApiService.class, GisLayerApiServiceImpl.class, GisLayerApiServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisLayerFileShapeService.class, GisLayerFileShapeServiceImpl.class, GisLayerFileShapeServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisLayerGroupService.class, GisLayerGroupServiceImpl.class, GisLayerGroupServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisLayerTablePostgisService.class, GisLayerTablePostgisServiceImpl.class, GisLayerTablePostgisServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisMapService.class, GisMapServiceImpl.class, GisMapServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisMapLayerService.class, GisMapLayerServiceImpl.class, GisMapLayerServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisMapServerService.class, GisMapServerServiceImpl.class, GisMapServerServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisMapStyleService.class, GisMapStyleServiceImpl.class, GisMapStyleServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisServerService.class, GisServerServiceImpl.class, GisServerServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisServerApiService.class, GisServerApiServiceImpl.class, GisServerApiServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisServerLayerService.class, GisServerLayerServiceImpl.class, GisServerLayerServiceMock.class, group, version, port);
		ServiceManager.me().mapping(GisStyleService.class, GisStyleServiceImpl.class, GisStyleServiceMock.class, group, version, port);
	}
	
	public void registerRemoteService() {
		registerRemoteService(getRpcGroup(), getRpcVersion(), getRpcPort());
	}
	
	public void registerRemoteService(String group, String version, int port) {
		ServiceManager.me().remote(GisApiService.class, GisApiServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisLayerService.class, GisLayerServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisLayerApiService.class, GisLayerApiServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisLayerFileShapeService.class, GisLayerFileShapeServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisLayerGroupService.class, GisLayerGroupServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisLayerTablePostgisService.class, GisLayerTablePostgisServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisMapService.class, GisMapServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisMapLayerService.class, GisMapLayerServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisMapServerService.class, GisMapServerServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisMapStyleService.class, GisMapStyleServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisServerService.class, GisServerServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisServerApiService.class, GisServerApiServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisServerLayerService.class, GisServerLayerServiceMock.class, group, version, port);
		ServiceManager.me().remote(GisStyleService.class, GisStyleServiceMock.class, group, version, port);
	}
	
	public int getRpcPort() {
		return Lambkit.config(RpcConfig.class).getDefaultPort();
	}
	public String getRpcGroup() {
		return Lambkit.config(RpcConfig.class).getDefaultGroup();
	}
	public String getRpcVersion() {
		return getConfig().getVersion();
	}
	public GisConfig getConfig() {
		return Lambkit.config(GisConfig.class);
	}
}

