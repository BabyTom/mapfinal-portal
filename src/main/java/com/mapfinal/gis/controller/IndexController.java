package com.mapfinal.gis.controller;

import com.mapfinal.gis.GisController;

public class IndexController extends GisController {

	/**
	 * 地图服务
	 */
	public void index() {
		renderTemplate("index.html");
	}
	
	/**
	 * 数据资源
	 */
	public void data() {
		renderTemplate("data.html");
	}
	
	/**
	 * 地理服务
	 */
	public void service() {
		renderTemplate("service.html");
	}
	
}
