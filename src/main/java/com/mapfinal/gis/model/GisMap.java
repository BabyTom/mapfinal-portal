/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model;

import java.io.File;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.lambkit.module.cms.core.template.TemplateManager;
import com.mapfinal.gis.GisConfig;
import com.mapfinal.gis.model.base.BaseGisMap;
import com.mapfinal.gis.model.sql.GisMapCriteria;
import com.mapfinal.gis.service.GisMapService;
import com.mapfinal.gis.service.impl.GisMapServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisMap extends BaseGisMap<GisMap> {

	private static final long serialVersionUID = 1L;
	
	public static GisMapService service() {
		return ServiceKit.inject(GisMapService.class, GisMapServiceImpl.class);
	}
	
	public static GisMapCriteria sql() {
		return new GisMapCriteria();
	}
	
	public static GisMapCriteria sql(Column column) {
		GisMapCriteria that = new GisMapCriteria();
		that.add(column);
        return that;
    }

	public GisMap() {
		GisConfig config = Lambkit.config(GisConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
	
	public String getWebPath() {
		return TemplateManager.me().getTemplatePath() + "/" + getTemplate();
	}
	
	public String getWebPath(String file) {
		return TemplateManager.me().getTemplatePath() + "/" + getTemplate() + "/" + file;
	}
	
	public String getFilePath(String file) {
		return PathKit.getWebRootPath() + TemplateManager.me().getTemplatePath() + File.separator + getTemplate() + File.separator + file;
	}
}
