/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseGisApi<M extends BaseGisApi<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "gis_api";
	}
    
	public java.lang.Long getId() {
		return this.get("id");
	}

	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.String getName() {
		return this.get("name");
	}

	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}

	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getUrl() {
		return this.get("url");
	}

	public void setUrl(java.lang.String url) {
		this.set("url", url);
	}
	public java.lang.String getSummary() {
		return this.get("summary");
	}

	public void setSummary(java.lang.String summary) {
		this.set("summary", summary);
	}
	public java.lang.String getOptions() {
		return this.get("options");
	}

	public void setOptions(java.lang.String options) {
		this.set("options", options);
	}
	public java.lang.String getType() {
		return this.get("type");
	}

	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getOrigin() {
		return this.get("origin");
	}

	public void setOrigin(java.lang.String origin) {
		this.set("origin", origin);
	}
	public java.lang.String getUrlpattern() {
		return this.get("urlpattern");
	}

	public void setUrlpattern(java.lang.String urlpattern) {
		this.set("urlpattern", urlpattern);
	}
	public java.lang.String getTargeturi() {
		return this.get("targetUri");
	}

	public void setTargeturi(java.lang.String targeturi) {
		this.set("targetUri", targeturi);
	}
	public java.lang.Integer getViewcount() {
		return this.get("viewcount");
	}

	public void setViewcount(java.lang.Integer viewcount) {
		this.set("viewcount", viewcount);
	}
	public java.lang.String getStatus() {
		return this.get("status");
	}

	public void setStatus(java.lang.String status) {
		this.set("status", status);
	}
}
