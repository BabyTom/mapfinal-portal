/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseGisLayer<M extends BaseGisLayer<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "gis_layer";
	}
    
	public java.lang.String getLayerkey() {
		return this.get("layerkey");
	}

	public void setLayerkey(java.lang.String layerkey) {
		this.set("layerkey", layerkey);
	}
	public java.lang.String getName() {
		return this.get("name");
	}

	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}

	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getGroups() {
		return this.get("groups");
	}

	public void setGroups(java.lang.String groups) {
		this.set("groups", groups);
	}
	public java.lang.String getDataType() {
		return this.get("data_type");
	}

	public void setDataType(java.lang.String dataType) {
		this.set("data_type", dataType);
	}
	public java.lang.String getStatus() {
		return this.get("status");
	}

	public void setStatus(java.lang.String status) {
		this.set("status", status);
	}
	public java.lang.String getThumbnail() {
		return this.get("thumbnail");
	}

	public void setThumbnail(java.lang.String thumbnail) {
		this.set("thumbnail", thumbnail);
	}
}
