/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisApiCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisApiCriteria create() {
		return new GisApiCriteria();
	}
	
	public static GisApiCriteria create(Column column) {
		GisApiCriteria that = new GisApiCriteria();
		that.add(column);
        return that;
    }

    public static GisApiCriteria create(String name, Object value) {
        return (GisApiCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_api", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisApiCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisApiCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisApiCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisApiCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisApiCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisApiCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisApiCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisApiCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisApiCriteria andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public GisApiCriteria andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public GisApiCriteria andIdIsEmpty() {
		empty("id");
		return this;
	}

	public GisApiCriteria andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
       public GisApiCriteria andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public GisApiCriteria andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public GisApiCriteria andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisApiCriteria andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public GisApiCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisApiCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisApiCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisApiCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisApiCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public GisApiCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public GisApiCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisApiCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisApiCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisApiCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisApiCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public GisApiCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public GisApiCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public GisApiCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public GisApiCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andSummaryIsNull() {
		isnull("summary");
		return this;
	}
	
	public GisApiCriteria andSummaryIsNotNull() {
		notNull("summary");
		return this;
	}
	
	public GisApiCriteria andSummaryIsEmpty() {
		empty("summary");
		return this;
	}

	public GisApiCriteria andSummaryIsNotEmpty() {
		notEmpty("summary");
		return this;
	}
        public GisApiCriteria andSummaryLike(java.lang.String value) {
    	   addCriterion("summary", value, ConditionMode.FUZZY, "summary", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andSummaryNotLike(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.NOT_FUZZY, "summary", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andSummaryEqualTo(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.EQUAL, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryNotEqualTo(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.NOT_EQUAL, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryGreaterThan(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.GREATER_THEN, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.GREATER_EQUAL, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryLessThan(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.LESS_THEN, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryLessThanOrEqualTo(java.lang.String value) {
          addCriterion("summary", value, ConditionMode.LESS_EQUAL, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("summary", value1, value2, ConditionMode.BETWEEN, "summary", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andSummaryNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("summary", value1, value2, ConditionMode.NOT_BETWEEN, "summary", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andSummaryIn(List<java.lang.String> values) {
          addCriterion("summary", values, ConditionMode.IN, "summary", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andSummaryNotIn(List<java.lang.String> values) {
          addCriterion("summary", values, ConditionMode.NOT_IN, "summary", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andOptionsIsNull() {
		isnull("options");
		return this;
	}
	
	public GisApiCriteria andOptionsIsNotNull() {
		notNull("options");
		return this;
	}
	
	public GisApiCriteria andOptionsIsEmpty() {
		empty("options");
		return this;
	}

	public GisApiCriteria andOptionsIsNotEmpty() {
		notEmpty("options");
		return this;
	}
        public GisApiCriteria andOptionsLike(java.lang.String value) {
    	   addCriterion("options", value, ConditionMode.FUZZY, "options", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andOptionsNotLike(java.lang.String value) {
          addCriterion("options", value, ConditionMode.NOT_FUZZY, "options", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andOptionsEqualTo(java.lang.String value) {
          addCriterion("options", value, ConditionMode.EQUAL, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsNotEqualTo(java.lang.String value) {
          addCriterion("options", value, ConditionMode.NOT_EQUAL, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsGreaterThan(java.lang.String value) {
          addCriterion("options", value, ConditionMode.GREATER_THEN, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("options", value, ConditionMode.GREATER_EQUAL, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsLessThan(java.lang.String value) {
          addCriterion("options", value, ConditionMode.LESS_THEN, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("options", value, ConditionMode.LESS_EQUAL, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("options", value1, value2, ConditionMode.BETWEEN, "options", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andOptionsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("options", value1, value2, ConditionMode.NOT_BETWEEN, "options", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andOptionsIn(List<java.lang.String> values) {
          addCriterion("options", values, ConditionMode.IN, "options", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOptionsNotIn(List<java.lang.String> values) {
          addCriterion("options", values, ConditionMode.NOT_IN, "options", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public GisApiCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public GisApiCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public GisApiCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
        public GisApiCriteria andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andOriginIsNull() {
		isnull("origin");
		return this;
	}
	
	public GisApiCriteria andOriginIsNotNull() {
		notNull("origin");
		return this;
	}
	
	public GisApiCriteria andOriginIsEmpty() {
		empty("origin");
		return this;
	}

	public GisApiCriteria andOriginIsNotEmpty() {
		notEmpty("origin");
		return this;
	}
        public GisApiCriteria andOriginLike(java.lang.String value) {
    	   addCriterion("origin", value, ConditionMode.FUZZY, "origin", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andOriginNotLike(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.NOT_FUZZY, "origin", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andOriginEqualTo(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.EQUAL, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginNotEqualTo(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.NOT_EQUAL, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginGreaterThan(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.GREATER_THEN, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.GREATER_EQUAL, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginLessThan(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.LESS_THEN, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginLessThanOrEqualTo(java.lang.String value) {
          addCriterion("origin", value, ConditionMode.LESS_EQUAL, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("origin", value1, value2, ConditionMode.BETWEEN, "origin", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andOriginNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("origin", value1, value2, ConditionMode.NOT_BETWEEN, "origin", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andOriginIn(List<java.lang.String> values) {
          addCriterion("origin", values, ConditionMode.IN, "origin", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andOriginNotIn(List<java.lang.String> values) {
          addCriterion("origin", values, ConditionMode.NOT_IN, "origin", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andUrlpatternIsNull() {
		isnull("urlpattern");
		return this;
	}
	
	public GisApiCriteria andUrlpatternIsNotNull() {
		notNull("urlpattern");
		return this;
	}
	
	public GisApiCriteria andUrlpatternIsEmpty() {
		empty("urlpattern");
		return this;
	}

	public GisApiCriteria andUrlpatternIsNotEmpty() {
		notEmpty("urlpattern");
		return this;
	}
        public GisApiCriteria andUrlpatternLike(java.lang.String value) {
    	   addCriterion("urlpattern", value, ConditionMode.FUZZY, "urlpattern", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andUrlpatternNotLike(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_FUZZY, "urlpattern", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andUrlpatternEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternNotEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternGreaterThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternLessThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternLessThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("urlpattern", value1, value2, ConditionMode.BETWEEN, "urlpattern", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andUrlpatternNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("urlpattern", value1, value2, ConditionMode.NOT_BETWEEN, "urlpattern", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andUrlpatternIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.IN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andUrlpatternNotIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.NOT_IN, "urlpattern", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andTargeturiIsNull() {
		isnull("targetUri");
		return this;
	}
	
	public GisApiCriteria andTargeturiIsNotNull() {
		notNull("targetUri");
		return this;
	}
	
	public GisApiCriteria andTargeturiIsEmpty() {
		empty("targetUri");
		return this;
	}

	public GisApiCriteria andTargeturiIsNotEmpty() {
		notEmpty("targetUri");
		return this;
	}
        public GisApiCriteria andTargeturiLike(java.lang.String value) {
    	   addCriterion("targetUri", value, ConditionMode.FUZZY, "targeturi", "java.lang.String", "String");
    	   return this;
      }

      public GisApiCriteria andTargeturiNotLike(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.NOT_FUZZY, "targeturi", "java.lang.String", "String");
          return this;
      }
      public GisApiCriteria andTargeturiEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiNotEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.NOT_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiGreaterThan(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.GREATER_THEN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.GREATER_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiLessThan(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.LESS_THEN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiLessThanOrEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.LESS_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("targetUri", value1, value2, ConditionMode.BETWEEN, "targeturi", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andTargeturiNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("targetUri", value1, value2, ConditionMode.NOT_BETWEEN, "targeturi", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andTargeturiIn(List<java.lang.String> values) {
          addCriterion("targetUri", values, ConditionMode.IN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andTargeturiNotIn(List<java.lang.String> values) {
          addCriterion("targetUri", values, ConditionMode.NOT_IN, "targeturi", "java.lang.String", "String");
          return this;
      }
	public GisApiCriteria andViewcountIsNull() {
		isnull("viewcount");
		return this;
	}
	
	public GisApiCriteria andViewcountIsNotNull() {
		notNull("viewcount");
		return this;
	}
	
	public GisApiCriteria andViewcountIsEmpty() {
		empty("viewcount");
		return this;
	}

	public GisApiCriteria andViewcountIsNotEmpty() {
		notEmpty("viewcount");
		return this;
	}
       public GisApiCriteria andViewcountEqualTo(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.EQUAL, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountNotEqualTo(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.NOT_EQUAL, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountGreaterThan(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.GREATER_THEN, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.GREATER_EQUAL, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountLessThan(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.LESS_THEN, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("viewcount", value, ConditionMode.LESS_EQUAL, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("viewcount", value1, value2, ConditionMode.BETWEEN, "viewcount", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisApiCriteria andViewcountNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("viewcount", value1, value2, ConditionMode.NOT_BETWEEN, "viewcount", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisApiCriteria andViewcountIn(List<java.lang.Integer> values) {
          addCriterion("viewcount", values, ConditionMode.IN, "viewcount", "java.lang.Integer", "Float");
          return this;
      }

      public GisApiCriteria andViewcountNotIn(List<java.lang.Integer> values) {
          addCriterion("viewcount", values, ConditionMode.NOT_IN, "viewcount", "java.lang.Integer", "Float");
          return this;
      }
	public GisApiCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public GisApiCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public GisApiCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public GisApiCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public GisApiCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "Float");
    	   return this;
      }

      public GisApiCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "Float");
          return this;
      }
      public GisApiCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public GisApiCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public GisApiCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public GisApiCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
}