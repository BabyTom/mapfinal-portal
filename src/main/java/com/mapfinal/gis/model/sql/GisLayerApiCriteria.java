/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisLayerApiCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisLayerApiCriteria create() {
		return new GisLayerApiCriteria();
	}
	
	public static GisLayerApiCriteria create(Column column) {
		GisLayerApiCriteria that = new GisLayerApiCriteria();
		that.add(column);
        return that;
    }

    public static GisLayerApiCriteria create(String name, Object value) {
        return (GisLayerApiCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer_api", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisLayerApiCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisLayerApiCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerApiCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisLayerApiCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisLayerApiCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisLayerApiCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisLayerApiCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisLayerApiCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisLayerApiCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public GisLayerApiCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public GisLayerApiCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public GisLayerApiCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public GisLayerApiCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerApiCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public GisLayerApiCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerApiCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerApiCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public GisLayerApiCriteria andApiIdIsNull() {
		isnull("api_id");
		return this;
	}
	
	public GisLayerApiCriteria andApiIdIsNotNull() {
		notNull("api_id");
		return this;
	}
	
	public GisLayerApiCriteria andApiIdIsEmpty() {
		empty("api_id");
		return this;
	}

	public GisLayerApiCriteria andApiIdIsNotEmpty() {
		notEmpty("api_id");
		return this;
	}
       public GisLayerApiCriteria andApiIdEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdNotEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.NOT_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdGreaterThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdLessThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("api_id", value1, value2, ConditionMode.BETWEEN, "apiId", "java.lang.Long", "Float");
    	  return this;
      }

      public GisLayerApiCriteria andApiIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("api_id", value1, value2, ConditionMode.NOT_BETWEEN, "apiId", "java.lang.Long", "Float");
          return this;
      }
        
      public GisLayerApiCriteria andApiIdIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.IN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerApiCriteria andApiIdNotIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.NOT_IN, "apiId", "java.lang.Long", "Float");
          return this;
      }
	public GisLayerApiCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public GisLayerApiCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public GisLayerApiCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public GisLayerApiCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
        public GisLayerApiCriteria andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "Float");
    	   return this;
      }

      public GisLayerApiCriteria andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "Float");
          return this;
      }
      public GisLayerApiCriteria andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerApiCriteria andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerApiCriteria andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public GisLayerApiCriteria andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
}