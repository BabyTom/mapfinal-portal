/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisLayerFileShapeCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisLayerFileShapeCriteria create() {
		return new GisLayerFileShapeCriteria();
	}
	
	public static GisLayerFileShapeCriteria create(Column column) {
		GisLayerFileShapeCriteria that = new GisLayerFileShapeCriteria();
		that.add(column);
        return that;
    }

    public static GisLayerFileShapeCriteria create(String name, Object value) {
        return (GisLayerFileShapeCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer_file_shape", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisLayerFileShapeCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisLayerFileShapeCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerFileShapeCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisLayerFileShapeCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisLayerFileShapeCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisLayerFileShapeCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisLayerFileShapeCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisLayerFileShapeCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisLayerFileShapeCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public GisLayerFileShapeCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public GisLayerFileShapeCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public GisLayerFileShapeCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public GisLayerFileShapeCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public GisLayerFileShapeCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public GisLayerFileShapeCriteria andSidIsNull() {
		isnull("sid");
		return this;
	}
	
	public GisLayerFileShapeCriteria andSidIsNotNull() {
		notNull("sid");
		return this;
	}
	
	public GisLayerFileShapeCriteria andSidIsEmpty() {
		empty("sid");
		return this;
	}

	public GisLayerFileShapeCriteria andSidIsNotEmpty() {
		notEmpty("sid");
		return this;
	}
       public GisLayerFileShapeCriteria andSidEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidNotEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.NOT_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidGreaterThan(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.GREATER_THEN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.GREATER_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidLessThan(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.LESS_THEN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.LESS_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("sid", value1, value2, ConditionMode.BETWEEN, "sid", "java.lang.Long", "Float");
    	  return this;
      }

      public GisLayerFileShapeCriteria andSidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("sid", value1, value2, ConditionMode.NOT_BETWEEN, "sid", "java.lang.Long", "Float");
          return this;
      }
        
      public GisLayerFileShapeCriteria andSidIn(List<java.lang.Long> values) {
          addCriterion("sid", values, ConditionMode.IN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andSidNotIn(List<java.lang.Long> values) {
          addCriterion("sid", values, ConditionMode.NOT_IN, "sid", "java.lang.Long", "Float");
          return this;
      }
	public GisLayerFileShapeCriteria andFileidIsNull() {
		isnull("fileid");
		return this;
	}
	
	public GisLayerFileShapeCriteria andFileidIsNotNull() {
		notNull("fileid");
		return this;
	}
	
	public GisLayerFileShapeCriteria andFileidIsEmpty() {
		empty("fileid");
		return this;
	}

	public GisLayerFileShapeCriteria andFileidIsNotEmpty() {
		notEmpty("fileid");
		return this;
	}
       public GisLayerFileShapeCriteria andFileidEqualTo(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.EQUAL, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidNotEqualTo(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.NOT_EQUAL, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidGreaterThan(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.GREATER_THEN, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.GREATER_EQUAL, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidLessThan(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.LESS_THEN, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("fileid", value, ConditionMode.LESS_EQUAL, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("fileid", value1, value2, ConditionMode.BETWEEN, "fileid", "java.lang.Long", "Float");
    	  return this;
      }

      public GisLayerFileShapeCriteria andFileidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("fileid", value1, value2, ConditionMode.NOT_BETWEEN, "fileid", "java.lang.Long", "Float");
          return this;
      }
        
      public GisLayerFileShapeCriteria andFileidIn(List<java.lang.Long> values) {
          addCriterion("fileid", values, ConditionMode.IN, "fileid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerFileShapeCriteria andFileidNotIn(List<java.lang.Long> values) {
          addCriterion("fileid", values, ConditionMode.NOT_IN, "fileid", "java.lang.Long", "Float");
          return this;
      }
	public GisLayerFileShapeCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisLayerFileShapeCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisLayerFileShapeCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisLayerFileShapeCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisLayerFileShapeCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public GisLayerFileShapeCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public GisLayerFileShapeCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisLayerFileShapeCriteria andGeofldIsNull() {
		isnull("geofld");
		return this;
	}
	
	public GisLayerFileShapeCriteria andGeofldIsNotNull() {
		notNull("geofld");
		return this;
	}
	
	public GisLayerFileShapeCriteria andGeofldIsEmpty() {
		empty("geofld");
		return this;
	}

	public GisLayerFileShapeCriteria andGeofldIsNotEmpty() {
		notEmpty("geofld");
		return this;
	}
        public GisLayerFileShapeCriteria andGeofldLike(java.lang.String value) {
    	   addCriterion("geofld", value, ConditionMode.FUZZY, "geofld", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerFileShapeCriteria andGeofldNotLike(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.NOT_FUZZY, "geofld", "java.lang.String", "String");
          return this;
      }
      public GisLayerFileShapeCriteria andGeofldEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldNotEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.NOT_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldGreaterThan(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.GREATER_THEN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.GREATER_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldLessThan(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.LESS_THEN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldLessThanOrEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.LESS_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("geofld", value1, value2, ConditionMode.BETWEEN, "geofld", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andGeofldNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("geofld", value1, value2, ConditionMode.NOT_BETWEEN, "geofld", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andGeofldIn(List<java.lang.String> values) {
          addCriterion("geofld", values, ConditionMode.IN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeofldNotIn(List<java.lang.String> values) {
          addCriterion("geofld", values, ConditionMode.NOT_IN, "geofld", "java.lang.String", "String");
          return this;
      }
	public GisLayerFileShapeCriteria andFldtypeIsNull() {
		isnull("fldtype");
		return this;
	}
	
	public GisLayerFileShapeCriteria andFldtypeIsNotNull() {
		notNull("fldtype");
		return this;
	}
	
	public GisLayerFileShapeCriteria andFldtypeIsEmpty() {
		empty("fldtype");
		return this;
	}

	public GisLayerFileShapeCriteria andFldtypeIsNotEmpty() {
		notEmpty("fldtype");
		return this;
	}
        public GisLayerFileShapeCriteria andFldtypeLike(java.lang.String value) {
    	   addCriterion("fldtype", value, ConditionMode.FUZZY, "fldtype", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerFileShapeCriteria andFldtypeNotLike(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.NOT_FUZZY, "fldtype", "java.lang.String", "String");
          return this;
      }
      public GisLayerFileShapeCriteria andFldtypeEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeNotEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.NOT_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeGreaterThan(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.GREATER_THEN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.GREATER_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeLessThan(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.LESS_THEN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.LESS_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fldtype", value1, value2, ConditionMode.BETWEEN, "fldtype", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andFldtypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fldtype", value1, value2, ConditionMode.NOT_BETWEEN, "fldtype", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andFldtypeIn(List<java.lang.String> values) {
          addCriterion("fldtype", values, ConditionMode.IN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andFldtypeNotIn(List<java.lang.String> values) {
          addCriterion("fldtype", values, ConditionMode.NOT_IN, "fldtype", "java.lang.String", "String");
          return this;
      }
	public GisLayerFileShapeCriteria andGeotypeIsNull() {
		isnull("geotype");
		return this;
	}
	
	public GisLayerFileShapeCriteria andGeotypeIsNotNull() {
		notNull("geotype");
		return this;
	}
	
	public GisLayerFileShapeCriteria andGeotypeIsEmpty() {
		empty("geotype");
		return this;
	}

	public GisLayerFileShapeCriteria andGeotypeIsNotEmpty() {
		notEmpty("geotype");
		return this;
	}
        public GisLayerFileShapeCriteria andGeotypeLike(java.lang.String value) {
    	   addCriterion("geotype", value, ConditionMode.FUZZY, "geotype", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerFileShapeCriteria andGeotypeNotLike(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.NOT_FUZZY, "geotype", "java.lang.String", "String");
          return this;
      }
      public GisLayerFileShapeCriteria andGeotypeEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeNotEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.NOT_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeGreaterThan(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.GREATER_THEN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.GREATER_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeLessThan(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.LESS_THEN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.LESS_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("geotype", value1, value2, ConditionMode.BETWEEN, "geotype", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andGeotypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("geotype", value1, value2, ConditionMode.NOT_BETWEEN, "geotype", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andGeotypeIn(List<java.lang.String> values) {
          addCriterion("geotype", values, ConditionMode.IN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andGeotypeNotIn(List<java.lang.String> values) {
          addCriterion("geotype", values, ConditionMode.NOT_IN, "geotype", "java.lang.String", "String");
          return this;
      }
	public GisLayerFileShapeCriteria andCrsIsNull() {
		isnull("crs");
		return this;
	}
	
	public GisLayerFileShapeCriteria andCrsIsNotNull() {
		notNull("crs");
		return this;
	}
	
	public GisLayerFileShapeCriteria andCrsIsEmpty() {
		empty("crs");
		return this;
	}

	public GisLayerFileShapeCriteria andCrsIsNotEmpty() {
		notEmpty("crs");
		return this;
	}
        public GisLayerFileShapeCriteria andCrsLike(java.lang.String value) {
    	   addCriterion("crs", value, ConditionMode.FUZZY, "crs", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerFileShapeCriteria andCrsNotLike(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.NOT_FUZZY, "crs", "java.lang.String", "String");
          return this;
      }
      public GisLayerFileShapeCriteria andCrsEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsNotEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.NOT_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsGreaterThan(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.GREATER_THEN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.GREATER_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsLessThan(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.LESS_THEN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.LESS_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("crs", value1, value2, ConditionMode.BETWEEN, "crs", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerFileShapeCriteria andCrsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("crs", value1, value2, ConditionMode.NOT_BETWEEN, "crs", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerFileShapeCriteria andCrsIn(List<java.lang.String> values) {
          addCriterion("crs", values, ConditionMode.IN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerFileShapeCriteria andCrsNotIn(List<java.lang.String> values) {
          addCriterion("crs", values, ConditionMode.NOT_IN, "crs", "java.lang.String", "String");
          return this;
      }
}