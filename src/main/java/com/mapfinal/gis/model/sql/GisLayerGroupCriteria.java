/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisLayerGroupCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisLayerGroupCriteria create() {
		return new GisLayerGroupCriteria();
	}
	
	public static GisLayerGroupCriteria create(Column column) {
		GisLayerGroupCriteria that = new GisLayerGroupCriteria();
		that.add(column);
        return that;
    }

    public static GisLayerGroupCriteria create(String name, Object value) {
        return (GisLayerGroupCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer_group", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisLayerGroupCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisLayerGroupCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerGroupCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisLayerGroupCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisLayerGroupCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisLayerGroupCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisLayerGroupCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisLayerGroupCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisLayerGroupCriteria andLayerGroupKeyIsNull() {
		isnull("layer_group_key");
		return this;
	}
	
	public GisLayerGroupCriteria andLayerGroupKeyIsNotNull() {
		notNull("layer_group_key");
		return this;
	}
	
	public GisLayerGroupCriteria andLayerGroupKeyIsEmpty() {
		empty("layer_group_key");
		return this;
	}

	public GisLayerGroupCriteria andLayerGroupKeyIsNotEmpty() {
		notEmpty("layer_group_key");
		return this;
	}
        public GisLayerGroupCriteria andLayerGroupKeyLike(java.lang.String value) {
    	   addCriterion("layer_group_key", value, ConditionMode.FUZZY, "layerGroupKey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyNotLike(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.NOT_FUZZY, "layerGroupKey", "java.lang.String", "String");
          return this;
      }
      public GisLayerGroupCriteria andLayerGroupKeyEqualTo(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.EQUAL, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyNotEqualTo(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.NOT_EQUAL, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyGreaterThan(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.GREATER_THEN, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.GREATER_EQUAL, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyLessThan(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.LESS_THEN, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_group_key", value, ConditionMode.LESS_EQUAL, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layer_group_key", value1, value2, ConditionMode.BETWEEN, "layerGroupKey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layer_group_key", value1, value2, ConditionMode.NOT_BETWEEN, "layerGroupKey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerGroupCriteria andLayerGroupKeyIn(List<java.lang.String> values) {
          addCriterion("layer_group_key", values, ConditionMode.IN, "layerGroupKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerGroupKeyNotIn(List<java.lang.String> values) {
          addCriterion("layer_group_key", values, ConditionMode.NOT_IN, "layerGroupKey", "java.lang.String", "String");
          return this;
      }
	public GisLayerGroupCriteria andLayerKeyIsNull() {
		isnull("layer_key");
		return this;
	}
	
	public GisLayerGroupCriteria andLayerKeyIsNotNull() {
		notNull("layer_key");
		return this;
	}
	
	public GisLayerGroupCriteria andLayerKeyIsEmpty() {
		empty("layer_key");
		return this;
	}

	public GisLayerGroupCriteria andLayerKeyIsNotEmpty() {
		notEmpty("layer_key");
		return this;
	}
        public GisLayerGroupCriteria andLayerKeyLike(java.lang.String value) {
    	   addCriterion("layer_key", value, ConditionMode.FUZZY, "layerKey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerGroupCriteria andLayerKeyNotLike(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_FUZZY, "layerKey", "java.lang.String", "String");
          return this;
      }
      public GisLayerGroupCriteria andLayerKeyEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyNotEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyGreaterThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyLessThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layer_key", value1, value2, ConditionMode.BETWEEN, "layerKey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerGroupCriteria andLayerKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layer_key", value1, value2, ConditionMode.NOT_BETWEEN, "layerKey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerGroupCriteria andLayerKeyIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.IN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andLayerKeyNotIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.NOT_IN, "layerKey", "java.lang.String", "String");
          return this;
      }
	public GisLayerGroupCriteria andOrdsIsNull() {
		isnull("ords");
		return this;
	}
	
	public GisLayerGroupCriteria andOrdsIsNotNull() {
		notNull("ords");
		return this;
	}
	
	public GisLayerGroupCriteria andOrdsIsEmpty() {
		empty("ords");
		return this;
	}

	public GisLayerGroupCriteria andOrdsIsNotEmpty() {
		notEmpty("ords");
		return this;
	}
        public GisLayerGroupCriteria andOrdsLike(java.lang.String value) {
    	   addCriterion("ords", value, ConditionMode.FUZZY, "ords", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerGroupCriteria andOrdsNotLike(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.NOT_FUZZY, "ords", "java.lang.String", "String");
          return this;
      }
      public GisLayerGroupCriteria andOrdsEqualTo(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.EQUAL, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsNotEqualTo(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.NOT_EQUAL, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsGreaterThan(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.GREATER_THEN, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.GREATER_EQUAL, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsLessThan(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.LESS_THEN, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ords", value, ConditionMode.LESS_EQUAL, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ords", value1, value2, ConditionMode.BETWEEN, "ords", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerGroupCriteria andOrdsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ords", value1, value2, ConditionMode.NOT_BETWEEN, "ords", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerGroupCriteria andOrdsIn(List<java.lang.String> values) {
          addCriterion("ords", values, ConditionMode.IN, "ords", "java.lang.String", "String");
          return this;
      }

      public GisLayerGroupCriteria andOrdsNotIn(List<java.lang.String> values) {
          addCriterion("ords", values, ConditionMode.NOT_IN, "ords", "java.lang.String", "String");
          return this;
      }
}