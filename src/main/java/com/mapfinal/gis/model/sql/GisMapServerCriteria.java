/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisMapServerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisMapServerCriteria create() {
		return new GisMapServerCriteria();
	}
	
	public static GisMapServerCriteria create(Column column) {
		GisMapServerCriteria that = new GisMapServerCriteria();
		that.add(column);
        return that;
    }

    public static GisMapServerCriteria create(String name, Object value) {
        return (GisMapServerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_map_server", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisMapServerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisMapServerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapServerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisMapServerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisMapServerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisMapServerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisMapServerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisMapServerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisMapServerCriteria andMapIdIsNull() {
		isnull("map_id");
		return this;
	}
	
	public GisMapServerCriteria andMapIdIsNotNull() {
		notNull("map_id");
		return this;
	}
	
	public GisMapServerCriteria andMapIdIsEmpty() {
		empty("map_id");
		return this;
	}

	public GisMapServerCriteria andMapIdIsNotEmpty() {
		notEmpty("map_id");
		return this;
	}
       public GisMapServerCriteria andMapIdEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdNotEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.NOT_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdGreaterThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdLessThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("map_id", value1, value2, ConditionMode.BETWEEN, "mapId", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisMapServerCriteria andMapIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("map_id", value1, value2, ConditionMode.NOT_BETWEEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisMapServerCriteria andMapIdIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapServerCriteria andMapIdNotIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.NOT_IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
	public GisMapServerCriteria andServerKeyIsNull() {
		isnull("server_key");
		return this;
	}
	
	public GisMapServerCriteria andServerKeyIsNotNull() {
		notNull("server_key");
		return this;
	}
	
	public GisMapServerCriteria andServerKeyIsEmpty() {
		empty("server_key");
		return this;
	}

	public GisMapServerCriteria andServerKeyIsNotEmpty() {
		notEmpty("server_key");
		return this;
	}
        public GisMapServerCriteria andServerKeyLike(java.lang.String value) {
    	   addCriterion("server_key", value, ConditionMode.FUZZY, "serverKey", "java.lang.String", "Float");
    	   return this;
      }

      public GisMapServerCriteria andServerKeyNotLike(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.NOT_FUZZY, "serverKey", "java.lang.String", "Float");
          return this;
      }
      public GisMapServerCriteria andServerKeyEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyNotEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.NOT_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyGreaterThan(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.GREATER_THEN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.GREATER_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyLessThan(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.LESS_THEN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.LESS_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("server_key", value1, value2, ConditionMode.BETWEEN, "serverKey", "java.lang.String", "String");
    	  return this;
      }

      public GisMapServerCriteria andServerKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("server_key", value1, value2, ConditionMode.NOT_BETWEEN, "serverKey", "java.lang.String", "String");
          return this;
      }
        
      public GisMapServerCriteria andServerKeyIn(List<java.lang.String> values) {
          addCriterion("server_key", values, ConditionMode.IN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisMapServerCriteria andServerKeyNotIn(List<java.lang.String> values) {
          addCriterion("server_key", values, ConditionMode.NOT_IN, "serverKey", "java.lang.String", "String");
          return this;
      }
}