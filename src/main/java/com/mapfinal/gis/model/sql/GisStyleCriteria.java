/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisStyleCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisStyleCriteria create() {
		return new GisStyleCriteria();
	}
	
	public static GisStyleCriteria create(Column column) {
		GisStyleCriteria that = new GisStyleCriteria();
		that.add(column);
        return that;
    }

    public static GisStyleCriteria create(String name, Object value) {
        return (GisStyleCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_style", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisStyleCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisStyleCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisStyleCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisStyleCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisStyleCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisStyleCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisStyleCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisStyleCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisStyleCriteria andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public GisStyleCriteria andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public GisStyleCriteria andIdIsEmpty() {
		empty("id");
		return this;
	}

	public GisStyleCriteria andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
       public GisStyleCriteria andIdEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdNotEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdGreaterThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdLessThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisStyleCriteria andIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisStyleCriteria andIdIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public GisStyleCriteria andIdNotIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Integer", "Float");
          return this;
      }
	public GisStyleCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisStyleCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisStyleCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisStyleCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisStyleCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public GisStyleCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public GisStyleCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisStyleCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisStyleCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisStyleCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisStyleCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andPathIsNull() {
		isnull("path");
		return this;
	}
	
	public GisStyleCriteria andPathIsNotNull() {
		notNull("path");
		return this;
	}
	
	public GisStyleCriteria andPathIsEmpty() {
		empty("path");
		return this;
	}

	public GisStyleCriteria andPathIsNotEmpty() {
		notEmpty("path");
		return this;
	}
        public GisStyleCriteria andPathLike(java.lang.String value) {
    	   addCriterion("path", value, ConditionMode.FUZZY, "path", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andPathNotLike(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_FUZZY, "path", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andPathEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathNotEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathGreaterThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathLessThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path", value1, value2, ConditionMode.BETWEEN, "path", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path", value1, value2, ConditionMode.NOT_BETWEEN, "path", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andPathIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.IN, "path", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andPathNotIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.NOT_IN, "path", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public GisStyleCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public GisStyleCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public GisStyleCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
        public GisStyleCriteria andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andOwnerIsNull() {
		isnull("owner");
		return this;
	}
	
	public GisStyleCriteria andOwnerIsNotNull() {
		notNull("owner");
		return this;
	}
	
	public GisStyleCriteria andOwnerIsEmpty() {
		empty("owner");
		return this;
	}

	public GisStyleCriteria andOwnerIsNotEmpty() {
		notEmpty("owner");
		return this;
	}
        public GisStyleCriteria andOwnerLike(java.lang.String value) {
    	   addCriterion("owner", value, ConditionMode.FUZZY, "owner", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andOwnerNotLike(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.NOT_FUZZY, "owner", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andOwnerEqualTo(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.EQUAL, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerNotEqualTo(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.NOT_EQUAL, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerGreaterThan(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.GREATER_THEN, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.GREATER_EQUAL, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerLessThan(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.LESS_THEN, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerLessThanOrEqualTo(java.lang.String value) {
          addCriterion("owner", value, ConditionMode.LESS_EQUAL, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("owner", value1, value2, ConditionMode.BETWEEN, "owner", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andOwnerNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("owner", value1, value2, ConditionMode.NOT_BETWEEN, "owner", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andOwnerIn(List<java.lang.String> values) {
          addCriterion("owner", values, ConditionMode.IN, "owner", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andOwnerNotIn(List<java.lang.String> values) {
          addCriterion("owner", values, ConditionMode.NOT_IN, "owner", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public GisStyleCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public GisStyleCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public GisStyleCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public GisStyleCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public GisStyleCriteria andSuffixIsNull() {
		isnull("suffix");
		return this;
	}
	
	public GisStyleCriteria andSuffixIsNotNull() {
		notNull("suffix");
		return this;
	}
	
	public GisStyleCriteria andSuffixIsEmpty() {
		empty("suffix");
		return this;
	}

	public GisStyleCriteria andSuffixIsNotEmpty() {
		notEmpty("suffix");
		return this;
	}
        public GisStyleCriteria andSuffixLike(java.lang.String value) {
    	   addCriterion("suffix", value, ConditionMode.FUZZY, "suffix", "java.lang.String", "String");
    	   return this;
      }

      public GisStyleCriteria andSuffixNotLike(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.NOT_FUZZY, "suffix", "java.lang.String", "String");
          return this;
      }
      public GisStyleCriteria andSuffixEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixNotEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.NOT_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixGreaterThan(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.GREATER_THEN, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.GREATER_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixLessThan(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.LESS_THEN, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixLessThanOrEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.LESS_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("suffix", value1, value2, ConditionMode.BETWEEN, "suffix", "java.lang.String", "String");
    	  return this;
      }

      public GisStyleCriteria andSuffixNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("suffix", value1, value2, ConditionMode.NOT_BETWEEN, "suffix", "java.lang.String", "String");
          return this;
      }
        
      public GisStyleCriteria andSuffixIn(List<java.lang.String> values) {
          addCriterion("suffix", values, ConditionMode.IN, "suffix", "java.lang.String", "String");
          return this;
      }

      public GisStyleCriteria andSuffixNotIn(List<java.lang.String> values) {
          addCriterion("suffix", values, ConditionMode.NOT_IN, "suffix", "java.lang.String", "String");
          return this;
      }
}