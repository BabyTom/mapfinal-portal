/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
public class LayerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static LayerCriteria create() {
		return new LayerCriteria();
	}
	
	public static LayerCriteria create(Column column) {
		LayerCriteria that = new LayerCriteria();
		that.add(column);
        return that;
    }

    public static LayerCriteria create(String name, Object value) {
        return (LayerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public LayerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public LayerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public LayerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public LayerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public LayerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public LayerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public LayerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public LayerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public LayerCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public LayerCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public LayerCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public LayerCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public LayerCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public LayerCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public LayerCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public LayerCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public LayerCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public LayerCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public LayerCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public LayerCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public LayerCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andGroupsIsNull() {
		isnull("groups");
		return this;
	}
	
	public LayerCriteria andGroupsIsNotNull() {
		notNull("groups");
		return this;
	}
	
	public LayerCriteria andGroupsIsEmpty() {
		empty("groups");
		return this;
	}

	public LayerCriteria andGroupsIsNotEmpty() {
		notEmpty("groups");
		return this;
	}
        public LayerCriteria andGroupsLike(java.lang.String value) {
    	   addCriterion("groups", value, ConditionMode.FUZZY, "groups", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andGroupsNotLike(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.NOT_FUZZY, "groups", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andGroupsEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsNotEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.NOT_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsGreaterThan(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.GREATER_THEN, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.GREATER_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsLessThan(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.LESS_THEN, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.LESS_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("groups", value1, value2, ConditionMode.BETWEEN, "groups", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andGroupsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("groups", value1, value2, ConditionMode.NOT_BETWEEN, "groups", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andGroupsIn(List<java.lang.String> values) {
          addCriterion("groups", values, ConditionMode.IN, "groups", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andGroupsNotIn(List<java.lang.String> values) {
          addCriterion("groups", values, ConditionMode.NOT_IN, "groups", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andDataTypeIsNull() {
		isnull("data_type");
		return this;
	}
	
	public LayerCriteria andDataTypeIsNotNull() {
		notNull("data_type");
		return this;
	}
	
	public LayerCriteria andDataTypeIsEmpty() {
		empty("data_type");
		return this;
	}

	public LayerCriteria andDataTypeIsNotEmpty() {
		notEmpty("data_type");
		return this;
	}
        public LayerCriteria andDataTypeLike(java.lang.String value) {
    	   addCriterion("data_type", value, ConditionMode.FUZZY, "dataType", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andDataTypeNotLike(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.NOT_FUZZY, "dataType", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andDataTypeEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeNotEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.NOT_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeGreaterThan(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.GREATER_THEN, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.GREATER_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeLessThan(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.LESS_THEN, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.LESS_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("data_type", value1, value2, ConditionMode.BETWEEN, "dataType", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andDataTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("data_type", value1, value2, ConditionMode.NOT_BETWEEN, "dataType", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andDataTypeIn(List<java.lang.String> values) {
          addCriterion("data_type", values, ConditionMode.IN, "dataType", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andDataTypeNotIn(List<java.lang.String> values) {
          addCriterion("data_type", values, ConditionMode.NOT_IN, "dataType", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public LayerCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public LayerCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public LayerCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public LayerCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public LayerCriteria andThumbnailIsNull() {
		isnull("thumbnail");
		return this;
	}
	
	public LayerCriteria andThumbnailIsNotNull() {
		notNull("thumbnail");
		return this;
	}
	
	public LayerCriteria andThumbnailIsEmpty() {
		empty("thumbnail");
		return this;
	}

	public LayerCriteria andThumbnailIsNotEmpty() {
		notEmpty("thumbnail");
		return this;
	}
        public LayerCriteria andThumbnailLike(java.lang.String value) {
    	   addCriterion("thumbnail", value, ConditionMode.FUZZY, "thumbnail", "java.lang.String", "String");
    	   return this;
      }

      public LayerCriteria andThumbnailNotLike(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.NOT_FUZZY, "thumbnail", "java.lang.String", "String");
          return this;
      }
      public LayerCriteria andThumbnailEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailNotEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.NOT_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailGreaterThan(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.GREATER_THEN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.GREATER_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailLessThan(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.LESS_THEN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailLessThanOrEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.LESS_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("thumbnail", value1, value2, ConditionMode.BETWEEN, "thumbnail", "java.lang.String", "String");
    	  return this;
      }

      public LayerCriteria andThumbnailNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("thumbnail", value1, value2, ConditionMode.NOT_BETWEEN, "thumbnail", "java.lang.String", "String");
          return this;
      }
        
      public LayerCriteria andThumbnailIn(List<java.lang.String> values) {
          addCriterion("thumbnail", values, ConditionMode.IN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public LayerCriteria andThumbnailNotIn(List<java.lang.String> values) {
          addCriterion("thumbnail", values, ConditionMode.NOT_IN, "thumbnail", "java.lang.String", "String");
          return this;
      }
}