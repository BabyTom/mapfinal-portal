/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.web.tag;

import java.io.IOException;
import java.util.Map;

import com.mapfinal.gis.model.GisLayerFileShape;
import com.mapfinal.gis.service.GisLayerFileShapeService;
import com.lambkit.common.util.StringUtils;
import com.lambkit.web.tag.LambkitTemplateModel;
import com.jfinal.kit.StrKit;
import com.jfinal.render.FreeMarkerRender;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
/**
 * gis_layer_file_shape标签<br>
 * 参数：{id:主键}
 * 返回值：{entity:gis_layer_file_shape信息}
 * @author lambkit
 */
public class GisLayerFileShapeTag extends LambkitTemplateModel {

	@Override
	public void onRender(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		// TODO Auto-generated method stub
		String layerkey = get(params, "layerkey");
		String sid = get(params, "sid");
		String fileid = get(params, "fileid");
		String name = get(params, "name");
		String geofld = get(params, "geofld");
		String fldtype = get(params, "fldtype");
		String geotype = get(params, "geotype");
		String crs = get(params, "crs");
		int pagenum = getInt(params, "pagenum", 0);
		int pagesize = getInt(params, "pagesize", 0);
		String wheresql = get(params, "sql", null);
		String sql = " from gis_layer_file_shape where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(layerkey)) sql += " and layerkey like '%" + layerkey + "%'";//varchar
			if(StringUtils.hasText(sid)) sql += " and sid=" + sid;//int8
			if(StringUtils.hasText(fileid)) sql += " and fileid=" + fileid;//int8
			if(StringUtils.hasText(name)) sql += " and name like '%" + name + "%'";//varchar
			if(StringUtils.hasText(geofld)) sql += " and geofld like '%" + geofld + "%'";//varchar
			if(StringUtils.hasText(fldtype)) sql += " and fldtype like '%" + fldtype + "%'";//varchar
			if(StringUtils.hasText(geotype)) sql += " and geotype like '%" + geotype + "%'";//varchar
			if(StringUtils.hasText(crs)) sql += " and crs like '%" + crs + "%'";//varchar
		} else {
			sql += wheresql;
		}
		
		String orderby = get(params, "orderby", null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		GisLayerFileShapeService service = GisLayerFileShape.service();
		
		String tagEntityKeyname = get(params, "key", "entity");
		if(pagenum==0) {
			env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
					service.dao().findFirst("select *" + sql)));
		} else {
			if(pagesize==0) {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().find("select *" + sql)));
			} else {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().paginate(pagenum, pagesize, "select *", sql)));
			}
		}
        body.render(env.getOut());
	}
}
