package com.mapfinal.pojo;

public class Bbox {

	private double minx;
	private double maxx;
	private double miny;
	private double maxy;
	
	public Bbox() {
		// TODO Auto-generated constructor stub
	}
	
	public Bbox(double minx, double miny, double maxx, double maxy) {
		// TODO Auto-generated constructor stub
		this.minx = minx;
		this.miny = miny;
		this.maxx = maxx;
		this.maxy = maxy;
	}
	
	public double getMinx() {
		return minx;
	}
	public void setMinx(double minx) {
		this.minx = minx;
	}
	public double getMaxx() {
		return maxx;
	}
	public void setMaxx(double maxx) {
		this.maxx = maxx;
	}
	public double getMiny() {
		return miny;
	}
	public void setMiny(double miny) {
		this.miny = miny;
	}
	public double getMaxy() {
		return maxy;
	}
	public void setMaxy(double maxy) {
		this.maxy = maxy;
	}
	
	
}
