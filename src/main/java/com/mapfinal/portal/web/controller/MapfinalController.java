package com.mapfinal.portal.web.controller;

import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.web.controller.LambkitController;

public class MapfinalController extends LambkitController {
	
	@Override
	public void render(String view) {
		super.render(TemplateManager.me().getCurrentWebPath(view));
	}
	
	@Override
	public void renderTemplate(String template) {
		super.renderTemplate(TemplateManager.me().getCurrentWebPath(template));
	}
	
	@Override
	public void renderFreeMarker(String view) {
		super.renderFreeMarker(TemplateManager.me().getCurrentWebPath(view));
	}
	
}
