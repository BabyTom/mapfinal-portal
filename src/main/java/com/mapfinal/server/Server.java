package com.mapfinal.server;

import java.util.Map;

import com.mapfinal.data.MapLayer;

/**
 * 地理服务器
 * @author yangyong
 *
 */
public interface Server {

	/**
	 * 主机地址
	 * @return
	 */
	String getHost();
	
	/**
	 * 是否是集群
	 * @return
	 */
	boolean isCluster();
	
	/**
	 * 发布图层
	 * @param layer
	 * @return
	 */
	boolean publish(MapLayer layer);
	
	/**
	 * 服务器上的图层列表
	 * @return
	 */
	Map<String, ServerLayer> getServerLayers();
	
	/**
	 * 加入图层
	 */
	void addServerLayer(ServerLayer serverLayer);
	
	/**
	 * 获取图层
	 * @param layerName
	 * @return
	 */
	ServerLayer getServerLayer(String layerName); 
	
	/**
	 * 是否有图层
	 * @param layerName
	 * @return
	 */
	boolean hasLayer(String layerName);
	
}
