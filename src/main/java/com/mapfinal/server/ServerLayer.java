package com.mapfinal.server;

import java.util.List;

/**
 * 服务器上的图层信息
 * @author yangyong
 *
 */
public interface ServerLayer {
	/**
	 * 图层名称
	 * @return
	 */
	String getName();
	/**
	 * 该服务上的图层名称
	 * @return
	 */
	String localName();
	/**
	 * 所属服务器
	 * @return
	 */
	Server getServer();
	
	/**
	 * 地图接口
	 * @return
	 */
	List<String> getMapApis();
}
