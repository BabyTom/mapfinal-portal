package com.mapfinal.server;

import java.util.Map;

import com.google.common.collect.Maps;
import com.lambkit.core.aop.AopKit;

public class ServerManager {
	private static ServerManager manager = null;

	public static ServerManager me() {
		if (manager == null) {
			manager = AopKit.singleton(ServerManager.class);
		}
		return manager;
	}
	
	private Map<String, Server> servers;
	
	public void addServer(String name, Server server) {
		if(servers==null) {
			servers = Maps.newHashMap();
		}
		servers.put(name, server);
	}

	public Map<String, Server> getServers() {
		return servers;
	}

	public void setServers(Map<String, Server> servers) {
		this.servers = servers;
	}

}
