package com.mapfinal.server.amap;

import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix="mapfinal.server.amap")
public class AMapConfig {
	
	private String name = "amap";
	private String version;
	private String url;
	private String key;
	private String sig;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSig() {
		return sig;
	}
	public void setSig(String sig) {
		this.sig = sig;
	}
}
