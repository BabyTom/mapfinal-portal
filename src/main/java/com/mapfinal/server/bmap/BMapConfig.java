package com.mapfinal.server.bmap;

import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix="mapfinal.server.bmap")
public class BMapConfig {
	
	private String name = "bmap";
	private String version = "v2";
	private String url = "http://api.map.baidu.com";
	private String ak;
	private String sn;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
}
