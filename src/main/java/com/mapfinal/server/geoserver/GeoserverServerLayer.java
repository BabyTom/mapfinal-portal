package com.mapfinal.server.geoserver;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

public class GeoserverServerLayer implements ServerLayer {

	private String name;
	private String localName;
	private GeoserverServer server;
	private List<String> mapApis;
	
	public static GeoserverServerLayer create(String localName, GeoserverServer server, MapLayer layer) {
		if(layer==null || server==null) return null;
		GeoserverServerLayer serverLayer = new GeoserverServerLayer();
		localName = StrKit.isBlank(localName) ? layer.getName() : localName;
		serverLayer.setLocalName(localName);
		serverLayer.setServer(server);
		layer.addServerLayer(serverLayer);
		server.addServerLayer(serverLayer);
		return serverLayer;
	}
	
	public void close() {
		server = null;
		if(mapApis!=null) mapApis.clear();
		mapApis = null;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String localName() {
		// TODO Auto-generated method stub
		return localName;
	}
	
	@Override
	public Server getServer() {
		// TODO Auto-generated method stub
		return server;
	}

	@Override
	public List<String> getMapApis() {
		// TODO Auto-generated method stub
		return mapApis;
	}

	public void setServer(GeoserverServer server) {
		this.server = server;
	}

	public void setMapApis(List<String> mapApis) {
		this.mapApis = mapApis;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}

}
