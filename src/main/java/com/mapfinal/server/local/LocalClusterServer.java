package com.mapfinal.server.local;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;
import com.mapfinal.server.geoserver.GeoserverConfig;

/**
 * JMS Cluster modules（集群数据同步），可用Geoserver管理
 * Nginx的集群数据不同步
 * @author yangyong
 *
 */
public class LocalClusterServer implements Server {
	
	private String name;
	private int rate = 0;//平均速度
	private Map<String, ServerLayer> serverLayers;
	private Map<String, LocalServer> servers = Maps.newHashMap();

	/**
	 * 最佳服务器
	 * @param layerName
	 * @return
	 */
	public LocalServer getBestServer(String layerName) {
		LocalServer dgs = null;
		for (LocalServer gs : servers.values()) {
			if (gs.hasLayer(layerName)) {
				if(dgs==null) {
					dgs = gs;
				} else if(dgs.getRate() < gs.getRate()) {
					dgs = gs;
				}
			}
		}
		return dgs;
	}
	
	/**
	 * 包含图层的所有服务器
	 * @param layerName
	 * @return
	 */
	public List<LocalServer> getServers(String layerName) {
		List<LocalServer> gslist = Lists.newArrayList();
		for (LocalServer gs : servers.values()) {
			if (gs.hasLayer(layerName)) {
				gslist.add(gs);
			}
		}
		return gslist;
	}
	
	/**
	 * 加入新的服务器
	 * @param server
	 */
	public void addServer(LocalServer server) {
		servers.put(server.getName(), server);
	}
	
	/**
	 * 默认服务器
	 * @return
	 */
	public LocalServer getDefaultServer() {
		return servers.get(GeoserverConfig.NAME_DEFAULT);
	}
	
	@Override
	public boolean publish(MapLayer layer) {
		// TODO Auto-generated method stub
		for (LocalServer gs : servers.values()) {
			gs.publish(layer);
		}
		return true;
	}
	
	@Override
	public boolean hasLayer(String layerName) {
		if(StrKit.isBlank(layerName)) return false;
		return serverLayers.containsKey(layerName);
	}
	
	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return getDefaultServer().getHost();
	}

	@Override
	public boolean isCluster() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void addServerLayer(ServerLayer serverLayer) {
		if (serverLayer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Maps.newHashMap();
		}
		serverLayers.put(serverLayer.getName(), serverLayer);
	}

	@Override
	public ServerLayer getServerLayer(String layerName) {
		// TODO Auto-generated method stub
		if (serverLayers == null) return null;
		return serverLayers.get(layerName);
	}
	
	@Override
	public Map<String, ServerLayer> getServerLayers() {
		// TODO Auto-generated method stub
		return serverLayers;
	}
	
	public void setServerLayers(Map<String, ServerLayer> serverLayers) {
		this.serverLayers = serverLayers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, LocalServer> getServers() {
		return servers;
	}

	public void setServers(Map<String, LocalServer> servers) {
		this.servers = servers;
	}
	
	public LocalServer getServer(String name) {
		return servers.get(name);
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}
}
