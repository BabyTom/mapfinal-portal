package com.mapfinal.server.local;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.data.MapStore;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

public class LocalServer implements Server {
	
	private boolean cluster;
	private String name;
	private LocalConfig config;
	private Map<String, MapStore> stores;
	private Map<String, ServerLayer> serverLayers;
	private int rate = 0;
	private boolean exist = false;
	
	public LocalServer() {
		this.setCluster(false);
	}
	
	public static LocalServer create(LocalConfig config) {
		if(config==null || !config.isConfigOK()) return null;
		LocalServer server = new LocalServer();
		server.setName(config.getName());
		server.setConfig(config);
		return server;
	}
	
	@Override
	public boolean publish(MapLayer layer) {
		// TODO Auto-generated method stub
		return false;
	}
	
	//-------------------------------------------
	
	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return config.getUrl();
	}

	@Override
	public boolean isCluster() {
		// TODO Auto-generated method stub
		return cluster;
	}
	
	@Override
	public boolean hasLayer(String layerName) {
		// TODO Auto-generated method stub
		if(StrKit.isBlank(layerName)) return false;
		return serverLayers.containsKey(layerName);
	}
	
	@Override
	public void addServerLayer(ServerLayer serverLayer) {
		if (serverLayer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Maps.newHashMap();
		}
		serverLayers.put(serverLayer.getName(), serverLayer);
	}
	
	@Override
	public ServerLayer getServerLayer(String layerName) {
		// TODO Auto-generated method stub
		if (serverLayers == null) return null;
		return serverLayers.get(layerName);
	}

	@Override
	public Map<String, ServerLayer> getServerLayers() {
		// TODO Auto-generated method stub
		return serverLayers;
	}
	
	public void setServerLayers(Map<String, ServerLayer> serverLayers) {
		this.serverLayers = serverLayers;
	}

	public void setCluster(boolean cluster) {
		this.cluster = cluster;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public boolean isExist() {
		return exist;
	}

	public void setExist(boolean exist) {
		this.exist = exist;
	}
	
	public void addStore(MapStore store) {
		if (store == null)
			return;
		if (stores == null) {
			stores = Maps.newHashMap();
		}
		stores.put(store.getName(), store);
	}
	
	public void addLayer(LocalServerLayer layer) {
		if (layer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Maps.newHashMap();
		}
		serverLayers.put(layer.getName(), layer);
	}
	
	public MapStore getStore(String name) {
		if(stores==null) return null;
		return stores.get(name);
	}
	
	public LocalServerLayer getLayer(String name) {
		if(serverLayers==null) return null;
		return (LocalServerLayer) serverLayers.get(name);
	}

	public Map<String, MapStore> getStores() {
		return stores;
	}

	public void setStores(Map<String, MapStore> stores) {
		this.stores = stores;
	}

	public LocalConfig getConfig() {
		return config;
	}

	public void setConfig(LocalConfig config) {
		this.config = config;
	}
	
}
