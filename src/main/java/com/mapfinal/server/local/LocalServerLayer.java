package com.mapfinal.server.local;

import org.geotools.map.Layer;
import org.geotools.map.MapContent;

import com.mapfinal.server.ServerLayer;

public abstract class LocalServerLayer implements ServerLayer {

	private MapContent map = null;

	public MapContent getMap() {
		return map;
	}
	
	public void addLayer(Layer layer) {
		if (map == null) {
			map = new MapContent();
		}
		// 承载各个Layer的Context
		map.addLayer(layer);
	}
}
