package com.mapfinal.server.tianditu;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

/**
 * 百度地图
 * @author yangyong
 *
 */
public class TiandituServer implements Server {

	private String name;
	private TiandituConfig config;
	private Map<String, ServerLayer> serverLayers;
	private int rate = 0;

	public static TiandituServer create(TiandituConfig config) {
		if (config == null)
			return null;
		TiandituServer server = new TiandituServer();
		server.setConfig(config);
		return server;
	}

	@Override
	public boolean publish(MapLayer layer) {
		// TODO Auto-generated method stub
		// not finish
		return false;
	}
	
	public boolean hasLayer(String layerName) {
		if(StrKit.isBlank(layerName)) return false;
		return serverLayers.containsKey(layerName);
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return config.getUrl();
	}

	@Override
	public boolean isCluster() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addServerLayer(ServerLayer serverLayer) {
		if (serverLayer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Maps.newHashMap();
		}
		serverLayers.put(serverLayer.getName(), serverLayer);
	}

	@Override
	public ServerLayer getServerLayer(String layerName) {
		// TODO Auto-generated method stub
		if (serverLayers == null) return null;
		return serverLayers.get(layerName);
	}
	
	@Override
	public Map<String, ServerLayer> getServerLayers() {
		// TODO Auto-generated method stub
		return serverLayers;
	}
	
	public void setServerLayers(Map<String, ServerLayer> serverLayers) {
		this.serverLayers = serverLayers;
	}

	public TiandituConfig getConfig() {
		return config;
	}

	public void setConfig(TiandituConfig config) {
		this.config = config;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	
}
