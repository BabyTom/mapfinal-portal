package com.lambkit.db.mgr.meta.test;

import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.lambkit.db.mgr.MgrdbConfig;
import com.lambkit.db.mgr.MgrdbManager;

/**
 * Sysconfig表格配置初始化
 * @author yangyong
 */
public class MgrInitTest {
	public static void main(String[] args) {
		Map<String, Object> options = Maps.newHashMap();
		//标题中需要去掉的前缀
		//options.put("tableRemovePrefixes", "cms_");
		//不包含如下数据表
		//options.put("excludedTables", "sys_fieldconfig, sys_tableconfig");
		//仅包含如下数据表
		options.put("includedTables",
				"gis_layer");
		//MgrdbManager.me().run(options, MgrdbConfig.SYSCONFIG);
		MgrdbManager.me().run(options, MgrdbConfig.ALL);
		System.exit(0);
	}
}
