package com.lambkit.db.mgr.meta.test;

import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.lambkit.db.mgr.MgrdbManager;

/**
 * Sysconfig表格配置初始化
 * @author yangyong
 */
public class SysconfigInitTest {
	public static void main(String[] args) {
		Map<String, Object> options = Maps.newHashMap();
		//标题中需要去掉的前缀
		options.put("tableRemovePrefixes", "gis_");
		//不包含如下数据表
		//options.put("excludedTables", "meta_correlation, bak_user,sys_fieldconfig, sys_tableconfig");
		//仅包含如下数据表
		options.put("includedTables","line_gkdy_qd,line_atmos_qd");
		MgrdbManager.me().run(options, "sysconfig");
		System.exit(0);
	}
}
