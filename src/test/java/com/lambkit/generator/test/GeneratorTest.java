package com.lambkit.generator.test;

import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.lambkit.generator.GeneratorType;
import com.lambkit.generator.GeneratorConfig;
import com.lambkit.generator.GeneratorManager;

public class GeneratorTest {

	public static void main(String[] args) {
		GeneratorConfig config = new GeneratorConfig();
		//生成java代码的存放地址
		config.setOutRootDir("D:/lambkit-workspace/git/gisfinal/src/main/java");
		//生成java代码的包地址
		config.setBasepackage("com.gisfinal.common");
		//生成前端文件文件夹
		config.setWebpages("app");
		//表格配置方式
		config.setMgrdb("normal");
		//选择一种模板语言
		config.setEngine(GeneratorConfig.TYPE_VELOCITY);
		//选择一种处理引擎
		config.setType(GeneratorType.DB.getValue());
		//表格
		Map<String,Object> options = Maps.newHashMap();
		//需要去掉的前缀
		options.put("tableRemovePrefixes", "data_");
		//仅包含如下表格
		//options.put("includedTables", "upms_favorites, upms_log, upms_organization, upms_permission, upms_role, upms_role_permission, upms_system, upms_tag, upms_user, upms_user_organization, upms_user_permission, upms_user_role");
		//options.put("includedTables", "meta_table, meta_field, meta_api, meta_app, meta_field_dimession, meta_field_edit, meta_field_list, meta_field_map, meta_field_measure, meta_field_relation, meta_file, meta_file_catalog, meta_file_catalog_mapping, meta_image, meta_image_set, meta_store, meta_store_db, meta_store_resource, meta_store_route, meta_theme");
		//options.put("includedTables", "meta_api, meta_app, meta_field_dimession, meta_field_edit, meta_field_list, meta_field_map, meta_field_measure, meta_field_relation, meta_file, meta_file_catalog, meta_file_catalog_mapping, meta_image, meta_image_set, meta_store, meta_store_db, meta_store_resource, meta_store_route, meta_theme");
		options.put("includedTables", "line_atmos_gkdy, line_atmos_qd, line_gkdy, line_gkdy_qd, line_land_ggzdqy, line_land_jsydwr, line_land_qkjqy, line_land_qtzddk, line_land_sgqy, line_land_szjcssyd, line_land_wkk, line_land_wrdk, line_land_wrsgdk, line_land_wxfwdccs, line_land_ybgk, line_land_zdgkqy, line_water_gkdy");
		//options.put("includedTables", "meta_image, meta_store_image_set, meta_store_route");
		options.put("hasMgrTable", true);
		//模板地址，根目录是项目文件夹
		String templatePath = "/template";
		GeneratorManager.me().run(templatePath, options, config);
		System.exit(0);
	}
}
