package com.lambkit.test.geo;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKBReader;

public class GeoTest {

	public static void main(String[] args) {
		String wkb = "0101000000410702092F745D406A14D4E8BCFA3F40";//POINT(117.8153708 31.97944503)
		byte[] aux = WKBReader.hexToBytes(wkb);
		try {
		    Geometry geom = new WKBReader().read(aux);
		    System.out.println(geom.toText());
		} catch (ParseException e) {
		    e.printStackTrace();
		    System.err.println("Bad WKB string.");
		}
	}
}
